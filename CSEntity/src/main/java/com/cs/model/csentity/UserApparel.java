package com.cs.model.csentity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the user_apparel database table.
 * 
 */
@Entity
@Table(name="user_apparel")
@NamedQuery(name="UserApparel.findAll", query="SELECT u FROM UserApparel u")
public class UserApparel implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	@Column(name="apparel_id")
	private Integer apparelId;

	private Integer lv;

	private Long uid;

	public UserApparel() 
	{
	}
	public UserApparel(Long uid, int apparelId) 
	{
		this.uid = uid;
		this.lv = 0;//服裝星等製作出來=0星
		this.apparelId = apparelId;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getApparelId() {
		return this.apparelId;
	}

	public void setApparelId(Integer apparelId) {
		this.apparelId = apparelId;
	}

	public Integer getLv() {
		return this.lv;
	}

	public void setLv(Integer lv) {
		this.lv = lv;
	}

	public Long getUid() {
		return this.uid;
	}

	public void setUid(Long uid) {
		this.uid = uid;
	}

}