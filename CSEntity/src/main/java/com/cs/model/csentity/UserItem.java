package com.cs.model.csentity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the user_item database table.
 * 
 */
@Entity
@Table(name="user_item")
@NamedQuery(name="UserItem.findAll", query="SELECT u FROM UserItem u")
public class UserItem implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	private Integer amount;

	@Column(name="item_id")
	private Integer itemId;

	private Long uid;

	public UserItem() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getAmount() {
		return this.amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public Integer getItemId() {
		return this.itemId;
	}

	public void setItemId(Integer itemId) {
		this.itemId = itemId;
	}

	public Long getUid() {
		return this.uid;
	}

	public void setUid(Long uid) {
		this.uid = uid;
	}

}