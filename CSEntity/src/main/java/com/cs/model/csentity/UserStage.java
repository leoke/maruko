package com.cs.model.csentity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the user_stage database table.
 * 
 */
@Entity
@Table(name="user_stage")
@NamedQuery(name="UserStage.findAll", query="SELECT u FROM UserStage u")
public class UserStage implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	private Integer score;

	@Column(name="stage_id")
	private Integer stageId;

	private Long uid;

	public UserStage() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getScore() {
		return this.score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public Integer getStageId() {
		return this.stageId;
	}

	public void setStageId(Integer stageId) {
		this.stageId = stageId;
	}

	public Long getUid() {
		return this.uid;
	}

	public void setUid(Long uid) {
		this.uid = uid;
	}

}