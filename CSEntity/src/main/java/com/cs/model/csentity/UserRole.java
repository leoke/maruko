package com.cs.model.csentity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the user_role database table.
 * 
 */
@Entity
@Table(name="user_role")
@NamedQuery(name="UserRole.findAll", query="SELECT u FROM UserRole u")
public class UserRole implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	private Integer lv;

	@Column(name="role_id")
	private Integer roleId;

	private Long uid;
	
	@Column(name="apparel_id")
	private Integer apparelId; 

	public UserRole(Integer lv,Integer roleId,Long uid,Integer apparelId) 
	{
		this.lv = lv;
		this.roleId = roleId;
		this.uid = uid;
		this.apparelId = apparelId;
	}

	public UserRole() 
	{
		
	}
	
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getLv() {
		return this.lv;
	}

	public void setLv(Integer lv) {
		this.lv = lv;
	}

	public Integer getRoleId() {
		return this.roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public Long getUid() {
		return this.uid;
	}

	public void setUid(Long uid) {
		this.uid = uid;
	}

	public Integer getApparelId() {
		return apparelId;
	}

	public void setApparelId(Integer apparelId) {
		this.apparelId = apparelId;
	}

	
}