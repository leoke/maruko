package com.cs.model.csentity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.*;


/**
 * The persistent class for the user database table.
 * 
 */
@Entity
@Table(name="user_data")
@NamedQuery(name="User.findAll", query="SELECT u FROM UserData AS u")
public class UserData implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="uid", unique=true, nullable=false)
	private Long uid;

	@Column(name="device_token", unique=true)
	private String deviceToken;
	
	@Column(name="fid", unique=true)
	private String fid;
	
	private Integer gcoin;
	//彈珠
	private Integer coin;
	
	private Timestamp login;

	@Column(name="login_mode")
	private Integer loginMode;

	private Integer ap;
	
	@Column(name="ap_time")
	private Timestamp apTime;
	
	@Column(name="tool")
	private String tool;
	
	@Column(name="buff")
	private String buff;
	
	@Column(name="status")//0_正常1_封鎖
	private Integer status;

//===================================================================================================================
	
	public UserData()
	{
		this.deviceToken="";
		this.fid="";
		this.ap=1000;
		this.coin=0;
		this.gcoin=0;
		this.tool="[0,0,0,0,0]";
		this.buff="[0,0,0]";
		this.status = 0;//預設0,1為封鎖
	}
	
//===================================================================================================================	
	public Long getUid() {
		return uid;
	}

	public void setUid(Long uid) {
		this.uid = uid;
	}

	public String getDeviceToken() {
		return deviceToken;
	}

	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}

	public String getFid() {
		return fid;
	}

	public void setFid(String fid) {
		this.fid = fid;
	}

	public Integer getGcoin() {
		return gcoin;
	}

	public void setGcoin(Integer gcoin) {
		this.gcoin = gcoin;
	}

	public Integer getCoin() {
		return coin;
	}

	public void setCoin(Integer coin) {
		this.coin = coin;
	}

	public Timestamp getLogin() {
		return login;
	}

	public void setLogin(Timestamp login) {
		this.login = login;
	}

	public Integer getLoginMode() {
		return loginMode;
	}

	public void setLoginMode(Integer loginMode) {
		this.loginMode = loginMode;
	}

	public Integer getAp() {
		return ap;
	}

	public void setAp(Integer ap) {
		this.ap = ap;
	}

	public Timestamp getApTime() {
		return apTime;
	}

	public void setApTime(Timestamp apTime) {
		this.apTime = apTime;
	}

	public String getTool() {
		return tool;
	}

	public void setTool(String tool) {
		this.tool = tool;
	}

	public String getBuff() {
		return buff;
	}
	
	public void setBuff(String buff) {
		this.buff = buff;
	}
	
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
	

}