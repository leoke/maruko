package com.cs.dao.csentity;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.cs.model.csentity.UserStage;


public interface UserStageDAO extends JpaRepository<UserStage, Long>
{
	
	@Query(value = " SELECT * FROM user_stage u WHERE u.uid = ?1 ", nativeQuery = true)
	public UserStage getOneStageByUid(Long uid);
	
	@Query(value = " SELECT * FROM user_stage u WHERE u.uid = ?1 AND stage_id = ?2 ", nativeQuery = true)
	public UserStage getOneStage(Long uid, int stageId);
	
	@Query(value = " SELECT * FROM user_stage u WHERE u.uid = ?1 AND stage_id BETWEEN ?2 AND ?3 ", nativeQuery = true)
	public List<UserStage> getList(Long uid,int StageIdStart, int StageIdEnd);
	
}
