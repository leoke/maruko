package com.cs.dao.csentity;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.cs.model.csentity.UserData;

public interface UserDAO extends JpaRepository<UserData, Long> {
	
	@Query(value = " SELECT count(u.uid) FROM user u WHERE u.uid = ?1 ", nativeQuery = true)
	public int findUidCount(Long userId);

	@Query(value = " SELECT count(u.device_token) FROM user_data u WHERE u.device_token = ?1 ", nativeQuery = true)
	public int findDeviceTokenCount(String deviceToken);
	
	@Query(value = " SELECT count(u.fid) FROM user_data u WHERE u.fid = ?1 ", nativeQuery = true)
	public int findFidCount(String fid);
	
	@Query(value = " SELECT * FROM user_data u WHERE u.fid = ?1 ", nativeQuery = true)
	public UserData getUserDataByFid(String fid);
	
	@Query(value = " SELECT * FROM user_data u WHERE u.device_token = ?1 ", nativeQuery = true)
	public UserData getUserDataByDeviceToken(String DeviceToken);
	
	@Query(value = " SELECT * FROM user_data u WHERE u.uid = ?1 ", nativeQuery = true)
	public UserData getUserDataByUid(Long uid);
	
//	@Query(value = " SELECT u FROM UserData u WHERE u.account = ?1 ")
//	public UserData findAccount(String account);
	
//	@Query(value = " SELECT u FROM UserData u WHERE u.account = ?1 AND u.loginMode =?2 ")
//	public UserData findAccountByLoginMode(String account,int loginMode);

//	public UserData findAccountByUid(String userId);

//	public UserData getUserByAccount(String account);

//	public List<UserData> geCtAllAccounts();

//	@Modifying
//	@Query(value = " UPDATE user u SET u.userName = ?2 WHERE u.account = ?1 ", nativeQuery = true)
//	public int updataUserName(String account, String userName);
}
