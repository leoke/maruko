package com.cs.dao.csentity;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.cs.model.csentity.UserData;

@Repository
public class UserDAOImpl {
	
	private static final Logger	LOG	= LoggerFactory.getLogger(UserDAOImpl.class);
	@PersistenceContext(unitName="game")
	@Qualifier(value = "entityManagerFactory")
	private EntityManager		entityManager;

//	public UserData findAccountByUid(String userId) {
//		final String SQL = " SELECT * FROM userData u WHERE u.userId = :userId ";
//		LOG.debug(" SQL :"+SQL + "  value:"+userId);
//		UserData userData = (UserData) entityManager.createNativeQuery(SQL , UserData.class).setParameter("userId", userId).getSingleResult();
//		return userData;
//	}
//
//	public UserData getUserByAccount(String account) {
//		return this.entityManager.createQuery("SELECT u FROM UserData u WHERE u.account = :account", UserData.class).setParameter("account", account).getSingleResult();
//	}
//
//	public List<UserData> getAllAccounts() {
//		return this.entityManager.createNamedQuery("User.findAll", UserData.class).getResultList();
//	}
	
	
	
}
