package com.cs.dao.csentity;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.cs.model.csentity.UserApparel;


public interface UserApparelDAO extends JpaRepository<UserApparel, Long>
{
	
	@Query(value = " SELECT * FROM user_apparel u WHERE u.uid = ?1 ", nativeQuery = true)
	public List<UserApparel> getAllUserApparel(Long uid); 
	
	@Query(value = " SELECT * FROM user_apparel u WHERE u.uid = ?1 AND u.apparel_id=?2", nativeQuery = true)
	public UserApparel getOneUserApparel(Long uid,Integer apparelId);
	
}
