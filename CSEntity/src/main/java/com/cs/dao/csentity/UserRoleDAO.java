package com.cs.dao.csentity;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.cs.model.csentity.UserRole;


public interface UserRoleDAO extends JpaRepository<UserRole, Long>
{
	
	@Query(value = " SELECT * FROM user_role u WHERE u.uid = ?1 ", nativeQuery = true)
	public List<UserRole> getAllUserRole(Long uid);
	
	@Query(value = " SELECT * FROM user_role u WHERE u.uid = ?1 AND role_id = ?2 ", nativeQuery = true)
	public UserRole getOneUserRole(Long uid, Integer roleId);
	
	@Query(value = " UPDATE user_role SET lv = ?3 WHERE uid = ?1 AND role_id = ?2 ", nativeQuery = true)
	public int UpdateRole(Long uid, Integer roleId, Integer lv );
	
//	@Query(value = " SELECT * FROM user_role u WHERE u.uid = ?1 AND role_id ?2", nativeQuery = true)
	public List<Integer>getRoleWearing(Long uid, String roleIds);
	
}
