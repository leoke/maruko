package com.cs.dao.csentity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

@Repository
public class UserStageDAOImpl 
{
	private static final Logger	LOG	= LoggerFactory.getLogger(UserStageDAOImpl.class);
	@PersistenceContext(unitName="game")
	@Qualifier(value = "entityManagerFactory")
	private EntityManager		entityManager;
}
