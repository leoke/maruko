package com.cs.dao.csentity;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.cs.model.csentity.UserItem;

public interface UserItemDAO extends JpaRepository<UserItem, Long>
{
	
	@Query(value = " SELECT * FROM user_item u WHERE u.uid = ?1 ", nativeQuery = true)
	public List<UserItem> getAllUserItem(Long uid);
	
	@Query(value = " SELECT * FROM user_item u WHERE u.uid = ?1 AND item_id = ?2", nativeQuery = true)
	public UserItem getOneUserItemByUid(Long uid, int itemId);
}
