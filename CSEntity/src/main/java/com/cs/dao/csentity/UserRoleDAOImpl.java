package com.cs.dao.csentity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.cs.model.csentity.UserRole;

@Repository
public class UserRoleDAOImpl 
{
	private static final Logger	LOG	= LoggerFactory.getLogger(UserRoleDAOImpl.class);
	@PersistenceContext(unitName="game")
	@Qualifier(value = "entityManagerFactory")
	private EntityManager		entityManager;
	
	
	public List<Integer> getRoleWearing(Long uid, String roleIds)
	{
		String sql = "SELECT apparel_id FROM user_role WHERE uid = "+uid+" AND role_id IN"+roleIds;
		List<Integer> rs = entityManager.createNativeQuery(sql).getResultList();
		
		if(rs == null)
		{
			return new ArrayList<Integer>();
		}
		return rs;
		
	}
}
