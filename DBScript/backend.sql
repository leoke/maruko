/*
Navicat MariaDB Data Transfer

Source Server         : MariaDB_localhost
Source Server Version : 100019
Source Host           : localhost:3306
Source Database       : backend

Target Server Type    : MariaDB
Target Server Version : 100019
File Encoding         : 65001

Date: 2016-07-11 17:38:28
*/

CREATE DATABASE IF NOT EXISTS `backenddb` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `backenddb`;

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for securitygroup
-- ----------------------------
DROP TABLE IF EXISTS `securitygroup`;
CREATE TABLE `securitygroup` (
  `groupId` int(11) NOT NULL,
  `groupName` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '群組名稱',
  `groupDesc` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '群組說明',
  `groupStatus` tinyint(1) NOT NULL DEFAULT '0' COMMENT '帳號狀態  0:封鎖 1:正常',
  `pages` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '功能列表清單',
  PRIMARY KEY (`groupId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of securitygroup
-- ----------------------------
INSERT INTO `securitygroup` VALUES ('1', 'ROLE_USER', null, '0', '');

-- ----------------------------
-- Table structure for securitypage
-- ----------------------------
DROP TABLE IF EXISTS `securitypage`;
CREATE TABLE `securitypage` (
  `pageId` int(11) NOT NULL AUTO_INCREMENT,
  `pageName` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT '功能名稱',
  `viewUrl` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT '頁面連結位置',
  `pageSwitch` tinyint(3) NOT NULL DEFAULT '0' COMMENT '功能開關 0:關閉 1:開啟',
  `type` tinyint(3) NOT NULL DEFAULT '0' COMMENT '功能狀態 0:選單 1:功能',
  `sort` tinyint(3) NOT NULL DEFAULT '0' COMMENT '功能顯示順序由低至高',
  `viewIcon` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '顯示的icon',
  PRIMARY KEY (`pageId`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of securitypage
-- ----------------------------
INSERT INTO `securitypage` VALUES ('1', '回首頁', 'index', '1', '0', '1', 'fa fa-home');
INSERT INTO `securitypage` VALUES ('2', '工具', '', '1', '0', '2', 'fa fa-wrench fa-fw');
INSERT INTO `securitypage` VALUES ('3', '表格', 'tables', '1', '2', '1', 'fa fa-table fa-fw');
INSERT INTO `securitypage` VALUES ('4', '表單', 'forms', '1', '2', '2', 'fa fa-edit fa-fw');
INSERT INTO `securitypage` VALUES ('5', 'icons', 'icons', '1', '2', '3', 'fa fa-gear');
INSERT INTO `securitypage` VALUES ('6', '權限設定', '', '1', '0', '3', 'fa glyphicon-cog');
INSERT INTO `securitypage` VALUES ('7', '功能列表', 'competence/action', '1', '3', '1', 'fa fa-list');
INSERT INTO `securitypage` VALUES ('8', '人員設定', 'competence/users', '1', '3', '2', 'fa fa-users');
INSERT INTO `securitypage` VALUES ('9', '排程資訊', 'competence/tasks', '1', '3', '3', 'fa glyphicon-tasks');

-- ----------------------------
-- Table structure for securityuser
-- ----------------------------
DROP TABLE IF EXISTS `securityuser`;
CREATE TABLE `securityuser` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '編號',
  `account` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '帳號',
  `name` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '使用者名稱',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '密碼',
  `regTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '註冊時間',
  `loginTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最後登入時間',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '帳號狀態  0:封鎖 1:正常 ',
  `group` int(11) NOT NULL DEFAULT '0' COMMENT '群組編號',
  `pages` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '功能列表清單',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of securityuser
-- ----------------------------
INSERT INTO `securityuser` VALUES ('1', 'SYSMAN', 'Admin', 'SYSMAN', '2016-07-04 16:01:42', '2016-07-18 10:21:03', '1', '1', '1,2,3,4,5,6,7,8,9');
