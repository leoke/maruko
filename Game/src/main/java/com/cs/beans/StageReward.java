package com.cs.beans;

/** 關卡獎勵資訊*/
public class StageReward 
{
	//道具名稱
	private int itemId;
	//數量
	private int amount;
	//掉落機率
	private int rate;
	public int getItemId() {
		return itemId;
	}
	public void setItemId(int itemId) {
		this.itemId = itemId;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public int getRate() {
		return rate;
	}
	public void setRate(int rate) {
		this.rate = rate;
	}
}
