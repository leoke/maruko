package com.cs.beans;

import java.util.ArrayList;

/** 角色服裝對應資訊*/
public class RoleApparelMapping 
{
	private int roleId;
	private ArrayList<Integer> condition;
	
	public int getRoleId() {
		return roleId;
	}
	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}
	public ArrayList<Integer> getCondition() {
		return condition;
	}
	public void setCondition(ArrayList<Integer> condition) {
		this.condition = condition;
	}
	
}
