package com.cs.beans;

/** 素材*/
public class ItemInfo 
{
	private int itemId;
	private int type;
	private int amount;
	private int price;
	
	public ItemInfo()
	{
		itemId=0;
		type=0;
		amount=0;
		price=0;
	}

	public int getItemId() {
		return itemId;
	}

	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}
	
}
