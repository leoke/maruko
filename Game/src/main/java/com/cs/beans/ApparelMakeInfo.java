package com.cs.beans;

/** 服裝製作所需的素材資訊*/
public class ApparelMakeInfo 
{
	private int itemId;
	private int type;
	private int amount;
	
	public ApparelMakeInfo()
	{
		itemId = 0;
		type = 0;
		amount = 0;
	}

	public int getItemId() {
		return itemId;
	}

	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}
	
}

