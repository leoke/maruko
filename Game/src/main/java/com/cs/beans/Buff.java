package com.cs.beans;

public class Buff {
	
	/** 流水號*/
	private int id;
	/** buff名稱*/
	private String name;
	/** buff價格*/
	private int price;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	
	
}
