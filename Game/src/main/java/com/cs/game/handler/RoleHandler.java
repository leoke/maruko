package com.cs.game.handler;

import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cs.beans.RoleApparelMapping;
import com.cs.game.data.dataPool.InitSystemDataPool;
import com.cs.game.db.service.UserApparelService;
import com.cs.game.db.service.UserDataService;
import com.cs.game.db.service.UserRoleService;
import com.cs.game.redis.beans.UserInfo;
import com.cs.game.redis.service.UserOperationsServiceImpl;
import com.cs.model.csentity.UserApparel;
import com.cs.model.csentity.UserData;
import com.cs.model.csentity.UserRole;

@Component("RoleHandler")
public class RoleHandler 
{
	private static final Logger	LOG	= LoggerFactory.getLogger(RoleHandler.class);
	
	@Autowired
	private UserRoleService userRoleService;
	@Autowired 
	private UserOperationsServiceImpl userOperationsService;
	@Autowired
	private UserApparelService userApparelService;
	@Autowired
	private InitSystemDataPool initSystemDataPool;
	@Autowired
	private UserDataService userDataService;
//===================================================================================================================

	/**
	 * 取得玩家角色資訊,無角色資料時顯示預設
	 * */
	public String getUserRoleInfo(Long uid)
	{
		JSONObject result = new JSONObject();
		result.put("rs", -1);
		List<UserRole> roleList = userRoleService.getAllUserRole(uid);
		if(roleList==null)
		{
			return result.toString();
		}
		result.put("rs", 0);
		result.put("data", JSONArray.fromObject(roleList));
//		userOperationsService.updateTokenTimeLimit(uid.toString());
		return result.toString();
	}
	
//===================================================================================================================

	/**
	 * 取得玩家角色資訊,無角色資料時顯示預設
	 * */
	public String getRoleWearing(Long uid, JSONArray roleIds)
	{
		JSONObject result = new JSONObject();
		
		String sql_condition = roleIds.toString().replace("[", "(").replace("]", ")");
		List<Integer> apparelIdTemp = userRoleService.getRoleWearing(uid, sql_condition);
		JSONArray rsData = new JSONArray();
		if(apparelIdTemp.isEmpty())
		{
			result.put("rs", 1);//無資料
			result.put("data", rsData);//無資料
			return result.toString();
		}
		for(int i=0;i<roleIds.size();i++)
		{
			rsData.add(0);
			RoleApparelMapping initData = initSystemDataPool.getRoleApparelMapping(roleIds.getInt(i));
			if(initData==null)
			{
				continue;
			}
			ArrayList<Integer> _condition = initData.getCondition();
			if(_condition==null)//沒有對應的資料
			{
				continue;
			}
			if( _condition.isEmpty())
			{
				continue;
			}
			for(Integer apId:apparelIdTemp)
			{
				if(_condition.contains(apId))
				{
					rsData.set(i, apId);
					break;
				}
			}
		}
		result.put("rs", 0);
		result.put("data", rsData);
		return result.toString();
	}
//===================================================================================================================	
	/** 新增角色
	 * @param Long uid 玩家編號
	 * @param int roleId 角色編號
	 * @param int apparelId 服裝編號
	 * @param int lv 角色等級
	 * */
	public String addRole(Long uid, int roleId, int apparelId,int lv)
	{
		JSONObject result = new JSONObject();
		result.put("rs", -1);
		UserRole role = userRoleService.getOneUserRole(uid, roleId);
		if(role!=null) result.put("rs", -2);//錯誤,已存在的角色
		role = new UserRole();
		role.setUid(uid);
		role.setRoleId(roleId);
		role.setApparelId(apparelId);
		role.setLv(1);
		userRoleService.addOrUpdateUserRole(role);
		result.put("rs", 0);//成功
		userOperationsService.updateTokenTimeLimit(uid.toString());
		return result.toString();
	}

	
//===================================================================================================================
	/**
	 * 修改角色升級
	 * @param Long uid 玩家編號
	 * @param int roleId 角色編號
	 * @param int apparelId 服裝編號
	 * @param int lv 角色等級
	 * */
	public String roleLevelUp(Long uid, int roleId, int apparelId)
	{
		JSONObject result = new JSONObject();
		result.put("rs", -1);//遊戲幣不足
		UserData userData = userDataService.getUserDataByUid(uid);
		
		if(userData==null)
		{
			result.put("rs", 1);//請重新登入
			return result.toString();
		}
		UserRole role = userRoleService.getOneUserRole(uid, roleId);
		Integer price = null;
		int level = 2;
		
		if(role==null)
		{
			role = new UserRole(1, roleId, uid, apparelId);
			price = initSystemDataPool.getRoleLevelMapping(level);
		}
		price = initSystemDataPool.getRoleLevelMapping(role.getLv()+1);
		if(price==null) 
		{
			result.put("rs", 2);//價格對應表找無資料
			return result.toString();
		}
		Integer gCoin = userData.getGcoin();
		if(gCoin<price)
		{
			result.put("rs", 3);
			return result.toString();//遊戲幣不足
		}
		userData.setGcoin(gCoin-price);
		role.setLv(role.getLv()+1);
		userRoleService.addOrUpdateUserRole(role);
		userDataService.addOrUpdateUser(userData);
		userOperationsService.putUser(userData);
		
		result.put("rs", 0);//成功
		result.put("gcoin", userData.getGcoin());
		result.put("roleLevel", role.getLv());
		userOperationsService.updateTokenTimeLimit(uid.toString());
		return result.toString();
	}
	
//===================================================================================================================

	/** 更換角色服裝
	 * @param Long uid 玩家編號
	 * @param int roleId 角色編號
	 * @param int apparelId 服裝編號
	 * @param int lv 角色等級
	 * */
	public String changeApparel(Long uid, int roleId, int apparelId)
	{
		JSONObject result = new JSONObject();
		result.put("rs", -1);
		UserRole role = userRoleService.getOneUserRole(uid, roleId);
		if(role==null)
		{
			int _lv = 1;//預設1級
			role = new UserRole(_lv, roleId, uid, apparelId);
		}
		UserApparel userApparel = userApparelService.getOneUserApparel(uid, apparelId);
		RoleApparelMapping mapping = initSystemDataPool.getRoleApparelMapping(roleId);
		if(userApparel ==null )
		{
			if(!mapping.getCondition().contains(apparelId)
				||
				!mapping.getCondition().get(0).equals(apparelId))
			{
				result.put("rs", 1);//無此服裝
				return result.toString();
			}
			userApparel = new UserApparel(uid, apparelId);
			userApparelService.addOuUpdate(userApparel);
		}
//		if(!mapping.getCondition().contains(apparelId))
//		{
//			result.put("rs", 2);//服裝無法對應
//			return result.toString();
//		}
		role.setApparelId(apparelId);
		userRoleService.addOrUpdateUserRole(role);
		result.put("rs", 0);//成功
		userOperationsService.updateTokenTimeLimit(uid.toString());
		return result.toString();
	}
//===================================================================================================================
	
	
}
