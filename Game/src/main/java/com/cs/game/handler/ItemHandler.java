package com.cs.game.handler;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cs.game.db.service.UserItemService;
import com.cs.model.csentity.UserItem;

@Component("ItemHandler")
public class ItemHandler 
{
	private static final Logger	LOG	= LoggerFactory.getLogger(ItemHandler.class);
	
	@Autowired
	private UserItemService userItemService;
	
	/** 取得所有玩家道具資訊*/
	public String getAllItemInfo(Long uid)
	{
		JSONObject rs = new JSONObject();
		rs.put("rs", 0);
		rs.put("data", JSONArray.fromObject(userItemService.getAllUserItem(uid)));
		return rs.toString();
	}
	
	
	
}
