package com.cs.game.handler;

import java.math.BigDecimal;
import java.util.ArrayList;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cs.game.data.dataPool.InitSystemDataPool;
import com.cs.game.db.service.UserStageService;
import com.cs.model.csentity.UserStage;

/** 關卡*/
@Component("StageHandler")
public class StageHandler 
{
	private static final Logger	LOG	= LoggerFactory.getLogger(StageHandler.class);
	private static final float BASIC = 0.01f;
	
	
	@Autowired
	private UserStageService userStageService;
	@Autowired
	private InitSystemDataPool initSystemDataPool;
	
//===================================================================================================================
	
	/**
	 * 取得玩家關卡資訊
	 * */
	public String getUserStageInfo(Long uid, int stageIdStart, int stageIdEnd)
	{
		JSONObject rs = new JSONObject();
		rs.put("rs", 0);
		rs.put("data", JSONArray.fromObject(userStageService.getList(uid, stageIdStart, stageIdEnd)));
		return rs.toString();
	}
//===================================================================================================================

	/**
	 * 更新關卡資訊
	 * */
	public void updateStage(Long uid, int stageId,int score)
	{
		UserStage userStage = userStageService.getOneStage(uid, stageId);
		if(userStage==null)
		{
			userStage = new UserStage();
			userStage.setUid(uid);
			userStage.setStageId(stageId);
			userStage.setScore(score);
		}
		userStage.setScore(score);
		userStageService.saveOrUpdate(userStage);
	}
	
//===================================================================================================================
	
	/** 取得關卡得分所對應的星等*/
	public int getScoreLevelMapping(int stageId, int score)
	{
		ArrayList<Integer> levelList = initSystemDataPool.getScoreLevelMap().get(stageId);
		if(levelList==null)return -1;//錯誤的關卡編號
		int max = levelList.get(levelList.size()-1);
		if(score>=max) return 3;//超過最高分,直接給予3星
		
		int s = 0;//星等
		for(Integer i :levelList)
		{
			BigDecimal b1 = new BigDecimal(BASIC);
			//百分比
			double p = b1.multiply(new BigDecimal(i)).divide(new BigDecimal(1), 2, BigDecimal.ROUND_HALF_UP).doubleValue();
			//目標分數
			int goal = new BigDecimal(p).multiply(new BigDecimal(max)).intValue();
			if(score<=goal)//玩家分數達到該星等分數需求
				s++;
		}
		return s;
	}
	
//	/** 分數轉換星等*/
//	public int parseScoreLevle()
//	{
//		
//	}
}
