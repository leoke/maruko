package com.cs.game.handler;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import org.springframework.stereotype.Component;

@Component("EncrypAES")
public class EncrypAES {
	
	/** KeyGenerator 提供對稱密鑰生成器的功能，支持各種演算法  */
	private static KeyGenerator Keygen;  
	/** SecretKey 負責保存對稱密鑰  */
	private static SecretKey Deskey;  
	/** Cipher負責完成加密或解密工作  */
	private static Cipher C;  
//	/** 該字節數組負責保存加密的結果  */
//	private static byte[] CipherByte;  
	
	static
	{
		 try {
			Keygen = KeyGenerator.getInstance("AES");
			Deskey = Keygen.generateKey();//生成密鑰   
			C = Cipher.getInstance("AES"); 
		}
		catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		catch (NoSuchPaddingException e) {
			e.printStackTrace();
		}
	}
	
	
	/** 
	 * 對字符串加密 
	 * 
	 * @param str 
	 * @return 
	 * @throws InvalidKeyException 
	 * @throws IllegalBlockSizeException 
	 * @throws BadPaddingException 
	 */  
	public byte[] Encrytor(String str)  
	    throws InvalidKeyException, IllegalBlockSizeException,  
	           BadPaddingException  
	{  
	    // 根據密鑰，對 Cipher 物件進行初始化，ENCRYPT_MODE 表示加密模式  
	    C.init(Cipher.ENCRYPT_MODE, Deskey);  
	    byte[] src = str.getBytes();  
//	    // 加密，結果保存進 cipherByte  
//	    cipherByte = C.doFinal(src);  
	    return C.doFinal(src);  
	}  
	
	/** 
	 * 對字符串解密 
	 * 
	 * @param buff 
	 * @return 
	 * @throws InvalidKeyException 
	 * @throws IllegalBlockSizeException 
	 * @throws BadPaddingException 
	 */  
	public byte[] Decryptor(byte[] buff)  
	    throws InvalidKeyException, IllegalBlockSizeException,  
	           BadPaddingException  
	{  
	    // 根據密鑰，對 Cipher 物件進行初始化，DECRYPT_MODE 表示加密模式  
	    C.init(Cipher.DECRYPT_MODE, Deskey);  
//	    cipherByte = c.doFinal(buff);  
	    return C.doFinal(buff);  
	}  
}
