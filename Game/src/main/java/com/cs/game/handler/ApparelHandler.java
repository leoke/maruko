package com.cs.game.handler;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cs.beans.ApparelMakeInfo;
import com.cs.game.data.dataPool.InitSystemDataPool;
import com.cs.game.db.service.UserApparelService;
import com.cs.game.db.service.UserDataService;
import com.cs.game.db.service.UserItemService;
import com.cs.game.redis.beans.UserInfo;
import com.cs.game.redis.service.UserOperationsServiceImpl;
import com.cs.model.csentity.UserApparel;
import com.cs.model.csentity.UserData;
import com.cs.model.csentity.UserItem;

/** 服裝*/
@Component("ApparelHandler")
public class ApparelHandler 
{
	private static final Logger	LOG	= LoggerFactory.getLogger(ApparelHandler.class);

	/** 服裝等級上限*/
	public static final int APPAREL_MAX_LEVEL = 4;
	
	
	/** mariaDB*/
	@Autowired 
	private UserDataService userDataService;
	/** redis*/
	@Autowired 
	private UserOperationsServiceImpl userOperationsService;
	@Autowired
	private UserApparelService userApparelService;
	@Autowired
	private UserItemService userItemService;
	@Autowired
	private InitSystemDataPool initSystemDataPool;
	@Autowired
	private UserHandler userHandler;
	
//===================================================================================================================		
	/**
	 * 取得玩家服裝資訊
	 * */
	public String getUserApparelInfo(Long uid)
	{
		JSONObject result = new JSONObject();
		result.put("rs", 0);
		List<UserApparel> apparelList = userApparelService.getAllUserApparel(uid);
		if(apparelList==null) return result.toString();
		result.put("data", JSONArray.fromObject(apparelList));
		return result.toString();
	}
	
//===================================================================================================================	
	/**
	 * 製作服裝
	 * */
	public String makeApparel(Long uid, int apparelId)
	{
		JSONObject result = new JSONObject();
		UserData userData = userDataService.getUserDataByUid(uid);
		if(userData==null)
		{
			result.put("rs", -1);//請重新登入
			return result.toString();
		}
		UserApparel apparel = userApparelService.getOneUserApparel(uid, apparelId);
		if((apparel!=null) || 
				initSystemDataPool.getAll_RoleBasicApparelMap().containsValue(apparelId))//排除預設的衣服不用製作
		{
			result.put("rs", 1);//已存在服裝,無法製作
			return result.toString();
		}
		ArrayList<ApparelMakeInfo> makeInfo = initSystemDataPool.getApparelMakeInfo(0, apparelId);
		if(makeInfo==null)
		{
			result.put("rs", 3);//查無對應的服裝製作資料
			return result.toString();
		}
		List<UserItem> userItemList = userItemService.getAllUserItem(uid);
		
		ArrayList<UserItem>  updateData = new ArrayList<UserItem>();
		for(ApparelMakeInfo _info:makeInfo)
		{
			int _type = _info.getType();//type 1:遊戲幣, 10 道具
			
			switch(_type)
			{
				case 1://type 1:遊戲幣
					if(_info.getAmount()>userData.getGcoin())
					{
						result.put("rs", 2);//遊戲幣數量不足
						return result.toString();
					}
					userData.setGcoin(userData.getGcoin()-_info.getAmount());
					break;
				case 10://type 10:道具
					for(UserItem _userItem:userItemList)
					{
						if(_userItem.getItemId().equals(_info.getItemId()))
						{
							if(_info.getAmount()>_userItem.getAmount())
							{
								result.put("rs", 3);//道具數量不足
								return result.toString();
							}
							_userItem.setAmount(_userItem.getAmount()-_info.getAmount());
							updateData.add(_userItem);
						}
					}
					break;
				default:
					break;
			}
			
			for(UserItem data:updateData)
			{
				userItemService.saveOrUpdate(data);
			}
		}
		apparel = new UserApparel(uid, apparelId);//產生服裝
		userApparelService.addOuUpdate(apparel);
		userOperationsService.putUser(userData);
		userDataService.addOrUpdateUser(userData);
		result.put("rs", 0);
		result.put("item", JSONArray.fromObject(updateData));
		result.put("apparel", JSONObject.fromObject(apparel));
		return result.toString();
	}
	
//===================================================================================================================	
	/**
	 * 服裝升級
	 * */
	public String apparelLevelUp(Long uid, int apparelId)
	{
		JSONObject result = new JSONObject();
		UserData userData = userDataService.getUserDataByUid(uid);
		if(userData==null)
		{
			result.put("rs", -1);//請重新登入
			return result.toString();
		}
		UserApparel apparel = userApparelService.getOneUserApparel(uid, apparelId);
		if(apparel==null)
		{
			if(!initSystemDataPool.getAll_RoleBasicApparelMap().containsValue(apparelId))
			{
				result.put("rs", 1);//不存在服裝,無法升級
				return result.toString();
			}
			apparel = new UserApparel(uid, apparelId);
		}
		if(apparel.getLv()>=APPAREL_MAX_LEVEL)
		{
			result.put("rs", 3);//無法升級,服裝等級已是最高
			return result.toString();
		}
		
		ArrayList<ApparelMakeInfo> makeInfo = initSystemDataPool.getApparelMakeInfo(apparel.getLv()+1, apparelId);
		List<UserItem> userItemList = userItemService.getAllUserItem(uid);
		
		ArrayList<UserItem>  updateData = new ArrayList<UserItem>();
		for(ApparelMakeInfo _info:makeInfo)
		{
			int _type = _info.getType();//type 1:遊戲幣, 10 道具
			
			switch(_type)
			{
				case 1://type 1:遊戲幣
					if(_info.getAmount()>userData.getGcoin())
					{
						result.put("rs", 2);//遊戲幣數量不足
						return result.toString();
					}
					userData.setGcoin(userData.getGcoin()-_info.getAmount());
					break;
				case 10://type 10:道具
					for(UserItem _userItem:userItemList)
					{
						if(_userItem.getItemId().equals(_info.getItemId()))
						{
							if(_info.getAmount()>_userItem.getAmount())
							{
								result.put("rs", 3);//道具數量不足
								return result.toString();
							}
							_userItem.setAmount(_userItem.getAmount()-_info.getAmount());
							updateData.add(_userItem);
						}
					}
					break;
				default:
					break;
			}
			
			for(UserItem data:updateData)
			{
				userItemService.saveOrUpdate(data);
			}
		}
		apparel.setLv(apparel.getLv()+1);//升星
		userApparelService.addOuUpdate(apparel);
		userOperationsService.putUser(userData);
		userDataService.addOrUpdateUser(userData);
		result.put("rs", 0);
		result.put("item", JSONArray.fromObject(updateData));
		result.put("apparel", JSONObject.fromObject(apparel));
		return result.toString();
	}
	
//===================================================================================================================
//	/**
//	 * 更換服裝 
//	 * */
//	public String changeApparel(Long uid, int roleId, int apparelId)
//	{
//		//要檢查該角色是否可以穿上此服裝
//		
//		return null;
//	}
	
//===================================================================================================================
	
	
//===================================================================================================================	
//===================================================================================================================	
//===================================================================================================================	
//===================================================================================================================	
//===================================================================================================================	
//===================================================================================================================	
//===================================================================================================================	
}
