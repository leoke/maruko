package com.cs.game.handler;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cs.beans.ApSellInfo;
import com.cs.beans.StageReward;
import com.cs.game.data.dataPool.InitSystemDataPool;
import com.cs.game.db.service.UserDataService;
import com.cs.game.db.service.UserItemService;
import com.cs.game.redis.beans.UserInfo;
import com.cs.game.redis.service.UserOperationsServiceImpl;
import com.cs.model.csentity.UserData;
import com.cs.model.csentity.UserItem;
import com.cs.util.DateUtil;
import com.cs.util.RanDomUtil;
import com.cs.util.TokenUtil;

@SuppressWarnings("static-access")
@Component("UserHandler")
public class UserHandler {
	
	private static final Logger	LOG	= LoggerFactory.getLogger(UserHandler.class);
	
	/** mariaDB*/
	@Autowired 
	private UserDataService userDataService;
	/** redis*/
	@Autowired 
	private UserOperationsServiceImpl userOperationsService;
	@Autowired
	private TokenUtil tokenUtil;
	@Autowired
	private InitSystemDataPool initSystemDataPool;
	@Autowired
	private StageHandler stageHandler;
	@Autowired
	private UserItemService userItemService;
	
//===================================================================================================================	
	
	/**
	 * 建立玩家帳號
	 * */
	public void createAccount(int type)
	{
		//建立帳號初始基本資訊,建立完成後直接登入?
		
		switch(type)
		{
			case 0 ://快速
				break;
			case 1 ://email
				
				break;
			case 2 ://FB
				break;
		}
	}

//===================================================================================================================	
	
	/** 
	 * 玩家登入
	 * 
	 * */
	public String login(JSONObject requestData)
	{
		JSONObject result = new JSONObject();
		String token = tokenUtil.getToken();
		UserData user = null;
		switch(requestData.getInt("loginMode"))//登入方式 
		{
			case 0://0_快速
				String deviceToken = requestData.getString("deviceToken");
				user = userDataService.getUserDataByDeviceToken(deviceToken);
				if(user!=null)
				{//有玩家資料
					if(user.getStatus()==1)
					{
						result.put("rs", -2);//-2被封鎖
						return result.toString();
					}
				}
				else
				{//建立新帳號
					user = new UserData();
					user.setLoginMode(0);
					user.setDeviceToken(deviceToken);
					user.setApTime(new Timestamp(System.currentTimeMillis()));
					userDataService.addOrUpdateUser(user);//to mariadb
				}
				userOperationsService.putUser(user,token);//to redis
				result.put("data", JSONObject.fromObject(new UserInfo(user)));
				result.put("token", token);
				result.put("rs", 0);//無異常
				return result.toString();
				
			case 1://1_FB
				String fid = requestData.getString("fid");
				user = userDataService.getUserDataByFid(fid);
				if(user!=null)
				{//有玩家資料
					
					//FB驗證放這----------------
					if(user.getStatus()==1)
					{
						result.put("rs", -2);//-2被封鎖
						return result.toString();
					}
				}
				else
				{//建立新帳號
					user = new UserData();
					user.setLoginMode(1);
					user.setFid(fid);
					user.setApTime(new Timestamp(System.currentTimeMillis()));
					userDataService.addOrUpdateUser(user);//to mariadb
				}
				userOperationsService.putUser(user, token);//to redis
				result.put("data", JSONObject.fromObject(new UserInfo(user)));
				result.put("rs", 0);//無異常
				result.put("token", token);
				return result.toString();
				
			default:
				result.put("rs", -1);
				break;
		}
		return result.toString();
	}
	
//===================================================================================================================	
	
	/**
	 * 更新token
	 * */
	public String updateToken(Long uid )
	{
		String newToken = tokenUtil.getToken();
		return userOperationsService.updateToken(uid.toString(), newToken);
	}
	
//===================================================================================================================	
	
	/** 購買BUFF*/
	public String buyBuff(Long uid,int packetIndex, int buffId)
	{
		JSONObject result = new JSONObject();
		result.put("rs", -1);
		ConcurrentHashMap<Integer, Integer> buffMap = initSystemDataPool.getBuffPriceMap();
		Integer price = buffMap.get(buffId);
		if(price==null)	return result.toString();//無對應價錢資料
		UserData userData = userOperationsService.getUser(uid.toString());
		if(userData==null)
		{
			result.put("rs", -2);//無玩家資料,請重新登入
			return result.toString();
		}
		Integer coin = userData.getCoin();
		if(price>coin)
		{
			result.put("rs", -3);//珍珠不夠
			return result.toString();
		}
		
		JSONArray buff = JSONArray.fromObject(userData.getBuff());
		if((int)buff.get(buffId-1) != 0)
		{
			result.put("rs", -4);//已持有buff,無法購買
			return result.toString();
		}
		userData.setCoin(coin-price<=0?0:coin-price);//扣錢
		buff.set(buffId-1, 1);//更改狀態,0未購買  1已購買
		userData.setBuff(buff.toString());
		userDataService.addOrUpdateUser(userData);
		userOperationsService.putUser(userData);
		result.put("rs", 0);
		userOperationsService.putLastPacket(uid.toString(), result.toString());
		return result.toString();
	}
	
//===================================================================================================================
	public String useBuff(Long uid,int packetIndex, int buffId)
	{
		JSONObject result = new JSONObject();
		result.put("rs", -1);
		UserData userData = userOperationsService.getUser(uid.toString());
		if(userData==null)
		{
			result.put("rs", -2);//無玩家資料,請重新登入
			return result.toString();
		}
		
		JSONArray buff = JSONArray.fromObject(userData.getBuff());
		int buffStatus;
		try
		{
			buffStatus = buff.getInt(buffId-1);
		}
		catch(IndexOutOfBoundsException e)
		{
			result.put("rs", -3);//index範圍超出
			return result.toString();
		}
		
		if(buffStatus<=0)
		{
			result.put("rs", -4);//數量不足,無法使用
			return result.toString();
		}
		
		buff.set(buffId-1, 0);
		userData.setBuff(buff.toString());
		userDataService.addOrUpdateUser(userData);
		userOperationsService.putUser(userData);
		result.put("rs", 0);//成功,已扣除flag
		userOperationsService.putLastPacket(uid.toString(), result.toString());
		return result.toString();
	}

//===================================================================================================================

	/**
	 * 開始關卡,buff在此作扣除
	 * 
	 * @return 0:成功,並扣除行動力
	 * */
	public String gameStart(Long uid, int stageId, JSONArray useBuff)
	{
		UserData userData = userOperationsService.getUser(uid.toString());
		JSONObject result = new JSONObject();
		if(userData==null)
		{
			result.put("rs", -1);//無玩家資料,請重新登入
			return result.toString();
		}
		
		//檢查AP
		if(userData.getAp()<initSystemDataPool.DeductionAp)
		{
			result.put("rs", 1);//AP不足
			return result.toString();
		}
		//扣除AP
		int _ap = userData.getAp()-initSystemDataPool.DeductionAp;
		userData.setAp(_ap<=0?0:_ap);
		//檢查是否要回復AP
		try
		{
			userData = checkAP(userData);
		}
		catch(ParseException pe)
		{
			result.put("rs", 2);//日期解析錯誤
			return result.toString(); 
		}
		//檢查buff
		JSONArray _userBuff = JSONArray.fromObject(userData.getBuff());
		for(int i = 0;i<useBuff.size();i++)
		{
			if((useBuff.getInt(i)!=0) && (_userBuff.getInt(i)<=0))
			{
				result.put("rs", 3);//buff數量錯誤
				return result.toString(); 
			}
			else if((useBuff.getInt(i)!=0) && (_userBuff.getInt(i)>0))//buff可用
			{
				_userBuff.set(i, _userBuff.getInt(i)-1);
			}
		}
		userData.setBuff(_userBuff.toString());
		userOperationsService.putUser(userData);
		userDataService.addOrUpdateUser(userData);
		//記憶關卡編號???
		result.put("rs", 0);//OK
		return result.toString();
	}
	
//===================================================================================================================
	/** 通關*/
	public String pass(Long uid, int stageId, int score)
	{
		//關卡flag要記錄??
		//通關獎勵掉落規則(星數  跟偉中討論
		JSONObject result = new JSONObject();
		if(!initSystemDataPool.getScoreLevelMap().containsKey(stageId))
		{
			result.put("rs", -1);
			return result.toString();//無此關卡編號
		}
		stageHandler.updateStage(uid, stageId, score);
		//星等
		int start = stageHandler.getScoreLevelMapping(stageId, score);
		JSONArray container = new JSONArray();
		if(start>0)
		{
			ArrayList<StageReward> reward = initSystemDataPool.getStageRewardMap(start,stageId);
			
			if(reward!=null)
			{
				//發獎
				for(StageReward r:reward)
				{
					JSONObject _reward = new JSONObject();
					UserItem userItem = userItemService.getOneUserItemByUid(uid, r.getItemId());
					if(!RanDomUtil.check(r.getRate()))break;//無獲得
					if(userItem ==null)
					{
						userItem = new UserItem();
						userItem.setUid(uid);
						userItem.setItemId(r.getItemId());
						userItem.setAmount(r.getAmount());
					}
					userItem.setAmount(userItem.getAmount()+(r.getAmount()<0?0:r.getAmount()));
					_reward.put("itemId", r.getItemId());
					_reward.put("amount", r.getAmount());
					container.add(_reward);
					userItemService.saveOrUpdate(userItem);
				}
			}
		}
		result.put("reward", container);//獲得道具
		result.put("rs", 0);//成功通關
		return result.toString();
	}
	
//===================================================================================================================
	/**
	 * 購買輔助道具
	 * @param Long uid 玩家編號
	 * @param int toolIndex 輔助道具的index,重0開始
	 * */
	public String buyTool(Long uid, int toolIndex , int amount)
	{
		JSONObject result = new JSONObject();
		UserData userData = userOperationsService.getUser(uid.toString());
		if(amount<=0)
		{
			result.put("rs", -2);//錯誤的數量
			return result.toString();
		}
		if(userData==null)
		{
			result.put("rs", -1);//無玩家資料,請重新登入
			return result.toString();
		}
		Integer price = initSystemDataPool.getToolPrice(toolIndex);
		int userCoin = userData.getCoin();//鑽石

		if(price==null)
		{
			result.put("rs", 1);//index無對應的價錢資料
			return result.toString();
		}
		price = price*amount;
		if(price>userCoin)
		{
			result.put("rs", 2);//鑽石不足
			return result.toString();
		}
		userData.setCoin(userCoin-price==0?0:userCoin-price);
		JSONArray tool = JSONArray.fromObject(userData.getTool());
		tool.set(toolIndex, tool.getInt(toolIndex)+1);
		userData.setTool(tool.toString());
		
		userOperationsService.putUser(userData);
		userDataService.addOrUpdateUser(userData);
		result.put("rs", 0);
		result.put("tool", tool);
		return result.toString();
	}
//===================================================================================================================
	/**
	 * 使用輔助道具
	 * @param Long uid 玩家編號
	 * @param int toolIndex 輔助道具的index,重0開始
	 * */
	public String useTool(Long uid, int toolIndex)
	{
		JSONObject result = new JSONObject();
		UserData userData = userOperationsService.getUser(uid.toString());
		if(userData==null)
		{
			result.put("rs", -1);//無玩家資料,請重新登入
			return result.toString();
		}
		JSONArray tool = JSONArray.fromObject(userData.getTool());
		int toolFlag = 0;
		try
		{
			toolFlag = tool.getInt(toolIndex);
		}
		catch(IndexOutOfBoundsException ioobe)
		{
			result.put("rs", 1);//錯誤的index
			return result.toString();
		}
		if(toolFlag<=0)
		{
			result.put("rs", 1);//數量不足,踢了他(有問題
			return result.toString();
		}
		tool.set(toolIndex, --toolFlag);
		userOperationsService.putUser(userData);
		userDataService.addOrUpdateUser(userData);
		result.put("rs", 0);
		result.put("tool", tool);
		return result.toString();
	}
	
	
	
//===================================================================================================================
	/**
	 * 檢查可回復的AP值
	 * @throws ParseException 
	 * */
	public UserData checkAP(UserData userData) throws ParseException
	{
		Timestamp last = userData.getApTime();
		long now = System.currentTimeMillis();
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(last.getTime());
		long lastTime = c.getTimeInMillis();
		long aptime = initSystemDataPool.RestoreAP_time;
		int restoreAP = initSystemDataPool.RestoreAP;
		while(lastTime<now)
		{
			if(((lastTime+aptime)>now) || userData.getAp()>=initSystemDataPool.AP_LIMIT)
			{
				userData.setApTime(new Timestamp(now));
				break;
			}
			lastTime = lastTime + aptime;
			int _ap = userData.getAp()+restoreAP;
			userData.setAp(_ap>=initSystemDataPool.AP_LIMIT?initSystemDataPool.AP_LIMIT:_ap);
			userData.setApTime(new Timestamp(now));
		}
		return userData;
	}
	
//===================================================================================================================
	/** 購買行動力*/
	public String buyAp(Long uid,int id)
	{
		JSONObject result = new JSONObject();
		UserData userData = userOperationsService.getUser(uid.toString());
		if(userData==null)
		{
			result.put("rs", -1);//無玩家資料,請重新登入
			return result.toString();
		}
		ApSellInfo sellInfo = initSystemDataPool.getApSellInfo(id);
		if(sellInfo==null)
		{
			result.put("rs", 1);//查無對應的販售資訊
			return result.toString();
		}
		if(sellInfo.getPrice()>userData.getCoin())
		{
			result.put("rs", 2);//鑽石不足
			return result.toString();
		}
		userData.setCoin(userData.getCoin()-sellInfo.getPrice());
		userData.setAp(userData.getAp()+sellInfo.getAp());
		userDataService.addOrUpdateUser(userData);
		userOperationsService.putUser(userData);
		result.put("rs", 0);
		result.put("coin", userData.getCoin());
		result.put("ap", userData.getAp());
		return result.toString();
	}
	
	
//===================================================================================================================
//===================================================================================================================
//===================================================================================================================
//===================================================================================================================
//===================================================================================================================
//===================================================================================================================
//===================================================================================================================
//===================================================================================================================
//===================================================================================================================
//===================================================================================================================
//===================================================================================================================
//===================================================================================================================
//===================================================================================================================
//===================================================================================================================
//===================================================================================================================
//===================================================================================================================
			

//	/**
//	 * 取得玩家道具資訊
//	 * */
//	public String getUserToolInfo(String uid,)
//	{
//		
//	}
	
//===================================================================================================================	
	/**
	 * 檢查token是否合法,true:可用,false:NO不可用
	 * */
	public boolean checkUserToken(String uid, String token)
	{
		String _token = userOperationsService.getToken(uid);
		if(_token==null)return false;
		if(!_token.equals(token))return false;
		return true;
	}
//===================================================================================================================
	
	/**
	 * 檢查該封包是否已處理過<br>
	 * null:無對應資料,可進行之後流程
	 * */
	public String checkPacket(String uid,int packetIndex)
	{
		String packet = userOperationsService.getLastPacket(uid);
		if(packet==null) return packet;
		JSONObject packetData = JSONObject.fromObject(packet);
		if(packetData.getInt("packetIndex")==packetIndex)
			return packetData.getJSONObject("packet").toString();
		return null;
	}
	
	
	public UserData parseUserInfo(UserInfo userInfo,UserData userData)
	{
		userData.setAp(userInfo.getAp());
		try {
			userData.setApTime(new Timestamp(DateUtil.SDF.parse(userInfo.getApTime()).getTime()));
		}
		catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
		userData.setAp(userInfo.getAp());
		userData.setBuff(userInfo.getBuff());
		userData.setTool(userInfo.getTool());
		userData.setCoin(userInfo.getCoin());
		userData.setGcoin(userInfo.getGcoin());
		return userData;
	}
	
//===================================================================================================================	
	
	
	
}
