package com.cs.game.db.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cs.dao.csentity.UserItemDAO;
import com.cs.model.csentity.UserItem;

@Service
@Transactional
public class UserItemServiceImpl implements UserItemService 
{
	private static final Logger	LOG	= LoggerFactory.getLogger(UserItemServiceImpl.class);
	
	@Autowired
	private UserItemDAO userItemDAO;
	
	
	
	@Override
	public void saveOrUpdate(UserItem userItem) 
	{
		userItemDAO.save(userItem);
	}
	
	@Override
	public List<UserItem> getAllUserItem(Long uid) 
	{
		return userItemDAO.getAllUserItem(uid);
	}

	@Override
	public UserItem getOneUserItemByUid(Long uid, int itemId) 
	{
		return userItemDAO.getOneUserItemByUid(uid, itemId);
	}

}
