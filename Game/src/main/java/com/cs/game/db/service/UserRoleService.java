package com.cs.game.db.service;

import java.util.List;

import net.sf.json.JSONArray;

import com.cs.model.csentity.UserRole;

public interface UserRoleService 
{
	
	public UserRole addOrUpdateUserRole(UserRole userRole);
	
	public void deleteUserRole(UserRole userRole);
	
	public List<UserRole> getAllUserRole(Long uid);
	
	public UserRole getOneUserRole(Long uid, Integer roleId);
	
	public List<Integer>getRoleWearing(Long uid, String roleIds);
	
	
}
