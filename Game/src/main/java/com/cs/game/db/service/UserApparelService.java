package com.cs.game.db.service;

import java.util.List;

import com.cs.model.csentity.UserApparel;

public interface UserApparelService 
{
	public void addOuUpdate(UserApparel userApparel);
	
	public List<UserApparel> getAllUserApparel(Long uid); 
	
	public UserApparel getOneUserApparel(Long uid, int apparelId); 
}
