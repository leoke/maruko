package com.cs.game.db.service;

import java.util.List;
import com.cs.model.csentity.UserItem;

public interface UserItemService 
{
	public void saveOrUpdate(UserItem userItem);
	
	public List<UserItem> getAllUserItem(Long uid);
	
	public UserItem getOneUserItemByUid(Long uid, int itemId);
	
	
}
