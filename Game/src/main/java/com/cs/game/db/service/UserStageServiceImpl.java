package com.cs.game.db.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cs.dao.csentity.UserStageDAO;
import com.cs.model.csentity.UserStage;

@Transactional
@Service
public class UserStageServiceImpl implements UserStageService 
{
	private static final Logger	LOG	= LoggerFactory.getLogger(UserStageServiceImpl.class);
	
	@Autowired
	private UserStageDAO userStageDAO;


	@Override
	public UserStage saveOrUpdate(UserStage userStage) {
		return userStageDAO.save(userStage);
	}
	
	@Override
	public UserStage getOneStageByUid(Long uid) {
		return userStageDAO.getOneStageByUid(uid);
	}

	@Override
	public UserStage getOneStage(Long uid, int stageId) {
		return userStageDAO.getOneStage(uid, stageId);
	}

	@Override
	public List<UserStage> getList(Long uid, int StageIdStart, int StageIdEnd) {
		return userStageDAO.getList(uid, StageIdStart, StageIdEnd);
	}

}

