package com.cs.game.db.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cs.dao.csentity.UserApparelDAO;
import com.cs.model.csentity.UserApparel;

@Transactional
@Service
public class UserApparelServiceImpl implements UserApparelService 
{
	private static final Logger	LOG	= LoggerFactory.getLogger(UserApparelService.class);
	
	@Autowired
	private UserApparelDAO userApparelDAO;

	@Override
	public List<UserApparel> getAllUserApparel(Long uid) {
		return userApparelDAO.getAllUserApparel(uid);
	}

	@Override
	public UserApparel getOneUserApparel(Long uid, int apparelId) {
		return userApparelDAO.getOneUserApparel(uid, apparelId);
	}

	@Override
	public void addOuUpdate(UserApparel userApparel) {
		userApparelDAO.save(userApparel);
	}
	
	
}

