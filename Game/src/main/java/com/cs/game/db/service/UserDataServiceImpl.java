package com.cs.game.db.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cs.dao.csentity.UserDAO;
import com.cs.model.csentity.UserData;

@Transactional
@Service
public class UserDataServiceImpl implements UserDataService {
	private static final Logger	LOG	= LoggerFactory.getLogger(UserDataServiceImpl.class);
	@Autowired
	private UserDAO				userDAO;

	@Override
	public int findUidCount(Long userId) {
		// TODO Auto-generated method stub
		int count = 0;
		try {
			count = userDAO.findUidCount(userId);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return count;
	}

	@Override
	public UserData addOrUpdateUser(UserData user) {
		return userDAO.save(user);
	}

	@Override
	public void delUser(UserData user) {
		userDAO.delete(user);
	}

	@Override
	public int findDeviceTokenCount(String deviceToken) {
		return userDAO.findDeviceTokenCount(deviceToken);
	}

	@Override
	public int findFidCount(String fid) {
		return userDAO.findFidCount(fid);
	}

	@Override
	public UserData getUserDataByFid(String fid) {
		return userDAO.getUserDataByFid(fid);
	}

	@Override
	public UserData getUserDataByDeviceToken(String DeviceToken) {
		return userDAO.getUserDataByDeviceToken(DeviceToken);
		
	}

	@Override
	public UserData getUserDataByUid(Long uid) {
		return userDAO.getUserDataByUid(uid);
	}
}
