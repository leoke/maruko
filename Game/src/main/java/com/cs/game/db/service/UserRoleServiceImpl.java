package com.cs.game.db.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cs.dao.csentity.UserRoleDAO;
import com.cs.model.csentity.UserRole;

@Service
@Transactional
public class UserRoleServiceImpl implements UserRoleService 
{
	private static final Logger	LOG	= LoggerFactory.getLogger(UserRoleServiceImpl.class);
	
	@Autowired
	private UserRoleDAO userRoleDAO;


	@Override
	public UserRole addOrUpdateUserRole(UserRole userRole) 
	{
		return userRoleDAO.save(userRole);
	}

	@Override
	public void deleteUserRole(UserRole userRole) 
	{
		 userRoleDAO.delete(userRole);
	}
	
	@Override
	public List<UserRole> getAllUserRole(Long uid) 
	{
		return userRoleDAO.getAllUserRole(uid);
	}

	@Override
	public UserRole getOneUserRole(Long uid, Integer roleId) 
	{
		return userRoleDAO.getOneUserRole(uid, roleId);
	}

	@Override
	public List<Integer> getRoleWearing(Long uid, String roleIds) {
		return userRoleDAO.getRoleWearing(uid, roleIds);
	}

	
	
	
}
