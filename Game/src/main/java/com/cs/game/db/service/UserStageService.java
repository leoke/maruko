package com.cs.game.db.service;

import java.util.List;

import com.cs.model.csentity.UserStage;

public interface UserStageService 
{
	
	public UserStage saveOrUpdate(UserStage userStage);
	
	public UserStage getOneStageByUid(Long uid);
	
	public UserStage getOneStage(Long uid, int stageId);
	
	public List<UserStage> getList(Long uid,int StageIdStart, int StageIdEnd);
	
}
