package com.cs.game.db.service;

import com.cs.model.csentity.UserData;

public interface UserDataService {
	/**
	 * 
	 * @param userId
	 * @return
	 */
	public int findUidCount(Long userId);

	public UserData addOrUpdateUser(UserData user);

	public void delUser(UserData user);

	public int findDeviceTokenCount(String deviceToken);
	
	public int findFidCount(String fid);
	
	public UserData getUserDataByFid(String fid);
	
	public UserData getUserDataByDeviceToken(String DeviceToken);
	
	public UserData getUserDataByUid(Long uid);
}
