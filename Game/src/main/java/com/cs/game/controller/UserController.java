package com.cs.game.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.cs.dao.csentity.UserDAOImpl;
import com.cs.game.redis.beans.UserInfo;
import com.cs.game.redis.service.UserOperationsServiceImpl;
import com.cs.model.csentity.UserData;
import com.cs.util.TokenUtil;



@Controller
@RequestMapping(value = "/redis")
public class UserController {
	private static final Logger	LOG	= LoggerFactory.getLogger(UserController.class);
	
	@Autowired
	private UserOperationsServiceImpl userOperationsService;
	@Autowired
	private TokenUtil tokenUtil;
//	private UserInfo user;

	@RequestMapping(value = "/addUser", method = RequestMethod.POST)
	public String addUser(HttpServletRequest request) {
		
		long id = Long.parseLong(request.getParameter("id"));
		int loginMode = Integer.parseInt(request.getParameter("loginMode"));
		int gcoin = Integer.parseInt(request.getParameter("gcoin"));
		int coin = Integer.parseInt(request.getParameter("coin"));
		int ap = Integer.parseInt(request.getParameter("ap"));
//		UserInfo user = new UserInfo();
		UserData user = new UserData(); 
		user.setUid(id);
		user.setLoginMode(loginMode);
		user.setGcoin(gcoin);
		user.setCoin(coin);
		user.setAp(ap);
		userOperationsService.putUser(user,tokenUtil.getToken());
		return "/AddUserSuccess";
	}

	@RequestMapping(value = "/addUser", method = RequestMethod.GET)
	public String addUser() {
		return "/AddUser";
	}

	@RequestMapping(value = "/queryUser")
	public String queryUser() {
		return "/getUser";
	}
	

	@RequestMapping(value = "/getUser")
	public String getUser(
			@RequestParam(value = "key", required = true) String key,Model model) {
//		user = userOperationsService.getUser(key);
		UserData user = userOperationsService.getUser(key);
		if(user != null){
			model.addAttribute("id", user.getUid());
			model.addAttribute("loginMode", user.getLoginMode());
			model.addAttribute("gcoin", user.getGcoin());
			model.addAttribute("coin", user.getCoin());
			model.addAttribute("ap", user.getAp());
			System.out.println(JSONObject.fromObject(user));
			model.addAttribute("apTime", user.getApTime());
		}else{
			model.addAttribute("userId", "null");
			model.addAttribute("username", "null");
			model.addAttribute("userpassword", "null");
		}
		

		return "/showUser";
	}
	
//	class ThreadExample1 extends Thread {
//		UserOperationsServiceImpl userOperationsService;
//		String Id;
//		
//		public ThreadExample1(UserOperationsServiceImpl userOperationsService, String Id){
//			this.userOperationsService = userOperationsService;
//			this.Id = Id;
//		}
//	    public void run() { // override Thread's run()
//	    	try{
//	    		Id = Id + this.getId();
//	    		System.out.println("-------------------------------------------------------------------------------------------------:");
//				Date startTime = new Date();
//				SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS");
//				SimpleDateFormat sdFormat2 = new SimpleDateFormat("yyyyMMddHHmmssSSS");
//				System.out.println("startTime:" + sdFormat.format(startTime));
//				for(int i=0; i<1000; i++){
//					user = new User(Id+i+sdFormat2.format(startTime), Id+i+sdFormat2.format(startTime), Id+i+sdFormat2.format(startTime));
//					userOperationsService.add(user);
//				}
//				Thread.sleep(5);
//				Date endTime = new Date();
//				System.out.println("endTime:" + sdFormat.format(endTime));
//				System.out.println("-------------------------------------------------------------------------------------------------:");
//	    	}catch(Exception e){
//	    		System.out.println(" Thread GG SO stop & back for foreach");
//	    	}
//	    	
//	    }
//	    
//	}
	
}
