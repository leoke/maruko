package com.cs.game.controller;

import java.io.IOException;
import java.sql.Timestamp;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cs.game.data.dataPool.UserDataPool;
import com.cs.game.db.service.UserApparelService;
import com.cs.game.db.service.UserDataService;
import com.cs.game.handler.ApparelHandler;
import com.cs.game.handler.ItemHandler;
import com.cs.game.handler.RoleHandler;
import com.cs.game.handler.StageHandler;
import com.cs.game.handler.UserHandler;
import com.cs.game.redis.beans.UserInfo;
import com.cs.game.redis.service.UserOperationsServiceImpl;
import com.cs.model.csentity.UserData;
import com.cs.util.Base64Util;
import com.cs.util.FileLoader;

@Controller
@RequestMapping("/game")
public class GameController {
	
	private static final Logger LOG = LoggerFactory.getLogger(GameController.class);
	
	@Autowired
	private UserDataService userDataService;
	@Autowired
	private UserOperationsServiceImpl userOperationsService;
	@Autowired
	private UserHandler userHandler;
	@Autowired
	private RoleHandler roleHandler;
	@Autowired
	private ApparelHandler apparelHandler;
	@Autowired
	private StageHandler stageHandler;
	@Autowired
	private ItemHandler itemHandler;
	@Autowired 
	private Base64Util base64Util;
	@Autowired 
	private UserDataPool userDataPool;
	@Autowired
	private FileLoader fileLoader;
	@Autowired
	private UserApparelService apparelService;
	
	@Value("${redis.host}")
	private String host;
	
	private static JSONObject ErrorMessage = new JSONObject();
	
	static
	{
		ErrorMessage.put("rs", -400);
	}
//===================================================================================================================	
//===================================================================================================================	
	/**
	 * 判斷封包是否已被處理過
	 * @param userId long 玩家編號
	 * @param packetId int 封包編號(client制定)
	 * @return 已被處理過回傳:JSONObject,否則回傳null 
	 * */
	@SuppressWarnings("unused")
	private JSONObject isAlreadySolved(long userId,int packetId)
	{
		return userDataPool.getPacketTempData(userId, packetId);
	}
	/**
	 * 新增封包至暫存區(已處理過的封包)
	 * @param userId long 玩家編號
	 * @param packetId int 封包編號(client制定)
	 * @param packet 封包
	 * */
	@SuppressWarnings("unused")
	private void addToPacketTemp(long userId,int packetId,JSONObject packet)
	{
		userDataPool.addDataToPacketTemp(userId, packetId, packet);
	}
//===================================================================================================================	
//===================================================================================================================	
	
	/** 登入遊戲*/
	@RequestMapping(value="/login" , method = RequestMethod.POST)
	public @ResponseBody String login(HttpServletRequest req)
	{
		JSONObject result = new JSONObject();
		try 
		{
			JSONObject data = JSONObject.fromObject(req.getAttribute("data"));
			return userHandler.login(data);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			result.put("rs", -1);//JSON格式無法解析
			return result.toString();
		}
	}
	
//===================================================================================================================
	/** 購買buff,不可堆疊,上限1,重複購買時將不會進行購買流程*/
	@RequestMapping(value="/buyBuff" , method = RequestMethod.POST)
	public @ResponseBody String buyBuff(HttpServletRequest req)
	{
		JSONObject data = JSONObject.fromObject(req.getAttribute("data"));
		Long uid = data.getLong("uid");
		int packetIndex = data.getInt("packetIndex");
		String token = data.getString("token");
		int buffId = data.getInt("buffId");
		if(!userHandler.checkUserToken(uid.toString(), token))
		{
			return ErrorMessage.toString();//不合法的token
		}
		String packet = userHandler.checkPacket(uid.toString(), packetIndex);
		if(packet!=null)//該編號之封包已處理過
		{
			return packet;
		}
		
		return userHandler.buyBuff(uid, packetIndex, buffId);
	}

//===================================================================================================================	
//	@RequestMapping(value="/useBuff" , method = RequestMethod.POST)
//	public @ResponseBody String useBuff(HttpServletRequest req)
//	{
//		JSONObject data = JSONObject.fromObject(req.getAttribute("data"));
//		Long uid = data.getLong("uid");
//		int packetIndex = data.getInt("packetIndex");
//		String token = data.getString("token");
//		int buffId = data.getInt("buffId");
//		if(!userHandler.checkUserToken(uid.toString(), token))
//		{
//			return ErrorMessage.toString();//不合法的token
//		}
//		String packet = userHandler.checkPacket(uid.toString(), packetIndex);
//		if(packet!=null)//該編號之封包已處理過
//		{
//			return packet;
//		}
//		return userHandler.useBuff(uid, packetIndex, buffId);
//	}
	
//===================================================================================================================	
	/** 取得角色資訊*/
	@RequestMapping(value="/getUserRoleInfo" , method = RequestMethod.POST)
	public @ResponseBody String getUserRoleInfo(HttpServletRequest req)
	{
		JSONObject data = JSONObject.fromObject(req.getAttribute("data"));
		Long uid = data.getLong("uid");
		String token = data.getString("token");
		if(!userHandler.checkUserToken(uid.toString(), token))
		{
			return ErrorMessage.toString();//不合法的token
		}
		return roleHandler.getUserRoleInfo(uid);
	}
	
//===================================================================================================================	
	/** 依角色編號取得角色資訊*/
	@RequestMapping(value="/getRoleWearing " , method = RequestMethod.POST)
	public @ResponseBody String getRoleWearing(HttpServletRequest req)
	{
		JSONObject data = JSONObject.fromObject(req.getAttribute("data"));
		Long uid = data.getLong("uid");
//		String token = data.getString("token");
		JSONArray roleIds = data.getJSONArray("roleIds");
		
//		if(!userHandler.checkUserToken(uid.toString(), token))
//		{
//			return ErrorMessage.toString();//不合法的token
//		}
		return roleHandler.getRoleWearing(uid, roleIds);
	}	
	
//===================================================================================================================	
	/** 新增角色(先不用給client)
	 * @param Long uid 玩家編號
	 * @param int roleId 角色編號
	 * @param int apparelId 服裝編號
	 * @param int lv 角色等級
	 * */	
	@RequestMapping(value="/addRole" , method = RequestMethod.POST)
	public @ResponseBody String addRole(HttpServletRequest req)
	{ 
		JSONObject data = JSONObject.fromObject(req.getAttribute("data"));
		Long uid = data.getLong("uid");
		String token = data.getString("token");
		int roleId = data.getInt("roleId");
		int apparelId = data.getInt("apparelId");
		int lv = data.getInt("lv");
		if(!userHandler.checkUserToken(uid.toString(), token))
		{
			return ErrorMessage.toString();//不合法的token
		}
		return roleHandler.addRole(uid, roleId, apparelId, lv);
	}
	
//===================================================================================================================
	/**
	 * 修改角色升級
	 * @param Long uid 玩家編號
	 * @param int roleId 角色編號
	 * @param int apparelId 服裝編號
	 * @param int lv 角色等級
	 * */
	@RequestMapping(value="/roleLevelUp" , method = RequestMethod.POST)
	public @ResponseBody String roleLevelUp(HttpServletRequest req)
	{ 
		JSONObject data = JSONObject.fromObject(req.getAttribute("data"));
		Long uid = data.getLong("uid");
		String token = data.getString("token");
		int roleId = data.getInt("roleId");
		int apparelId = data.getInt("apparelId");
//		int lv = data.getInt("lv");
		if(!userHandler.checkUserToken(uid.toString(), token))
		{
			return ErrorMessage.toString();//不合法的token
		}
		return roleHandler.roleLevelUp(uid, roleId, apparelId);
	}
	
//===================================================================================================================
	/** 更換角色服裝
	 * @param Long uid 玩家編號
	 * @param int roleId 角色編號
	 * @param int apparelId 服裝編號
	 * @param int lv 角色等級
	 * */
	@RequestMapping(value="/changeApparel" , method = RequestMethod.POST)
	public @ResponseBody String changeApparel(HttpServletRequest req)
	{ 
		JSONObject data = JSONObject.fromObject(req.getAttribute("data"));
		Long uid = data.getLong("uid");
		String token = data.getString("token");
		int roleId = data.getInt("roleId");
		int apparelId = data.getInt("apparelId");
		int lv = data.getInt("lv");
		if(!userHandler.checkUserToken(uid.toString(), token))
		{
			return ErrorMessage.toString();//不合法的token
		}
		return roleHandler.changeApparel(uid, roleId, apparelId);
	}
	
//===================================================================================================================
	/** 取得玩家服裝資訊*/
	@RequestMapping(value="/getUserApparelInfo" , method = RequestMethod.POST)
	public @ResponseBody String getUserApparelInfo(HttpServletRequest req)
	{ 
		JSONObject data = JSONObject.fromObject(req.getAttribute("data"));
		Long uid = data.getLong("uid");
		String token = data.getString("token");
		if(!userHandler.checkUserToken(uid.toString(), token))
		{
			return ErrorMessage.toString();//不合法的token
		}
		return apparelHandler.getUserApparelInfo(uid);
	}
	
//===================================================================================================================
	/** 取得所有玩家道具資訊*/
	@RequestMapping(value="/getAllItemInfo" , method = RequestMethod.POST)
	public @ResponseBody String getAllItemInfo(HttpServletRequest req)
	{ 
		JSONObject data = JSONObject.fromObject(req.getAttribute("data"));
		Long uid = data.getLong("uid");
		String token = data.getString("token");
		if(!userHandler.checkUserToken(uid.toString(), token))
		{
			return ErrorMessage.toString();//不合法的token
		}
		return itemHandler.getAllItemInfo(uid);
	}
	
//===================================================================================================================
	/** 取得玩家關卡資訊*/
	@RequestMapping(value="/getUserStageInfo" , method = RequestMethod.POST)
	public @ResponseBody String getUserStageInfo(HttpServletRequest req)
	{ 
		JSONObject data = JSONObject.fromObject(req.getAttribute("data"));
		Long uid = data.getLong("uid");
		String token = data.getString("token");
		if(!userHandler.checkUserToken(uid.toString(), token))
		{
			return ErrorMessage.toString();//不合法的token
		}
		int stageIdStart = data.getInt("stageIdStart");
		int stageIdEnd = data.getInt("stageIdEnd");
		return stageHandler.getUserStageInfo(uid, stageIdStart, stageIdEnd);
	}
	
//===================================================================================================================
	/** 開始遊戲*/
	@RequestMapping(value="/gameStart" , method = RequestMethod.POST)
	public @ResponseBody String gameStart(HttpServletRequest req)
	{ 
		JSONObject data = JSONObject.fromObject(req.getAttribute("data"));
		Long uid = data.getLong("uid");
		String token = data.getString("token");
		if(!userHandler.checkUserToken(uid.toString(), token))
		{
			return ErrorMessage.toString();//不合法的token
		}
		int stageId = data.getInt("stageId");
		JSONArray useBuff = data.getJSONArray("useBuff");
		return userHandler.gameStart(uid, stageId, useBuff);
	}
	
//===================================================================================================================
	/** 過關回報*/
	@RequestMapping(value="/pass" , method = RequestMethod.POST)
	public @ResponseBody String pass(HttpServletRequest req)
	{ 
		JSONObject data = JSONObject.fromObject(req.getAttribute("data"));
		Long uid = data.getLong("uid");
		String token = data.getString("token");
		if(!userHandler.checkUserToken(uid.toString(), token))
		{
			return ErrorMessage.toString();//不合法的token
		}
		int stageId = data.getInt("stageId");
		int score = data.getInt("score");
		return userHandler.pass(uid, stageId, score);
	}

//===================================================================================================================
	/** 購買輔助道具*/
	@RequestMapping(value="/buyTool" , method = RequestMethod.POST)
	public @ResponseBody String buyTool(HttpServletRequest req)
	{ 
		JSONObject data = JSONObject.fromObject(req.getAttribute("data"));
		Long uid = data.getLong("uid");
		String token = data.getString("token");
		if(!userHandler.checkUserToken(uid.toString(), token))
		{
			return ErrorMessage.toString();//不合法的token
		}
		int toolIndex = data.getInt("toolIndex");
		int amount = data.getInt("amount");
		return userHandler.buyTool(uid, toolIndex,amount);
		
	}
	
//===================================================================================================================
	/** 使用輔助道具*/
	@RequestMapping(value="/useTool" , method = RequestMethod.POST)
	public @ResponseBody String useTool(HttpServletRequest req)
	{ 
		JSONObject data = JSONObject.fromObject(req.getAttribute("data"));
		Long uid = data.getLong("uid");
		String token = data.getString("token");
		if(!userHandler.checkUserToken(uid.toString(), token))
		{
			return ErrorMessage.toString();//不合法的token
		}
		int toolIndex = data.getInt("toolIndex");
		return userHandler.useTool(uid, toolIndex);
		
	}	
//===================================================================================================================
	/** 製作服裝*/
	@RequestMapping(value="/makeApparel" , method = RequestMethod.POST)
	public @ResponseBody String makeApparel(HttpServletRequest req)
	{ 
		JSONObject data = JSONObject.fromObject(req.getAttribute("data"));
		Long uid = data.getLong("uid");
		String token = data.getString("token");
		if(!userHandler.checkUserToken(uid.toString(), token))
		{
			return ErrorMessage.toString();//不合法的token
		}
		int apparelId = data.getInt("apparelId");
		return apparelHandler.makeApparel(uid, apparelId);
	}
	
//===================================================================================================================
	/** 升級服裝*/
	@RequestMapping(value="/apparelLevelUp" , method = RequestMethod.POST)
	public @ResponseBody String apparelLevelUp(HttpServletRequest req)
	{ 
		JSONObject data = JSONObject.fromObject(req.getAttribute("data"));
		Long uid = data.getLong("uid");
		String token = data.getString("token");
		if(!userHandler.checkUserToken(uid.toString(), token))
		{
			return ErrorMessage.toString();//不合法的token
		}
		int apparelId = data.getInt("apparelId");
		return apparelHandler.apparelLevelUp(uid, apparelId);
	}	
//===================================================================================================================	
	/** 購買行動力*/
	@RequestMapping(value="/buyAP" , method = RequestMethod.POST)
	public @ResponseBody String buyAP(HttpServletRequest req)
	{
		JSONObject data = JSONObject.fromObject(req.getAttribute("data"));
		Long uid = data.getLong("uid");
		String token = data.getString("token");
		if(!userHandler.checkUserToken(uid.toString(), token))
		{
			return ErrorMessage.toString();//不合法的token
		}
		int id = data.getInt("id");
		return userHandler.buyAp(uid, id);
	}

//===================================================================================================================
	
//===================================================================================================================
//===================================================================================================================
//===================================================================================================================
//===================================================================================================================	
	/** 取得好友資訊*/
	@RequestMapping(value="/getFriendsInfo" , method = RequestMethod.POST)
	public @ResponseBody String getFriendsInfo(HttpServletRequest req)
	{
		return null;
	}
	
//===================================================================================================================	
	
	/** 綁定FB*/
	@RequestMapping(value="/bindingFB" , method = RequestMethod.POST)
	public @ResponseBody String bindingFB(HttpServletRequest req)
	{
		
		return null;
	}

//===================================================================================================================	
	/** 更新token有效時間並回傳新的token給client*/
	@RequestMapping(value="/updateToken" , method = RequestMethod.POST)
	public @ResponseBody String updateToken(HttpServletRequest req)
	{
		
		return null;
	}
	
//===================================================================================================================
	
	

	
//===================================================================================================================	
	@RequestMapping(value="/test" , method = RequestMethod.POST)
	public @ResponseBody String test(HttpServletRequest req) throws IOException
	{
//		fileLoader.test("test.txt");
//		System.out.println("token-->> "+userOperationsService.getToken(1+"token"));
//		System.out.println("token-->> "+userOperationsService.getUser("1"));
//		System.out.println("token-->> "+userOperationsService.getUser("1"));
		
		
//		return userHandler.buyBuff("1", 1, 1);
//		req.getParameter("uid");
//		return userHandler.useBuff(req.getParameter("uid"), 0, Integer.parseInt(req.getParameter("buffId")));
//		apparelService.getAllUserApparel(1l);
		
		
		JSONObject data = JSONObject.fromObject(req.getAttribute("data"));
		Long uid = data.getLong("uid");
		String token = data.getString("token");
		JSONArray roleIds = data.getJSONArray("roleIds");
		return roleHandler.getRoleWearing(uid, roleIds);
	}
	
}
