package com.cs.game.data.dataPool;

import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;

import net.sf.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.cs.model.csentity.UserData;

@Component("UserDataPool")
public class UserDataPool {
	
	private static final Logger	LOG	= LoggerFactory.getLogger(UserDataPool.class);
	
	/** 玩家token*/
	private static ConcurrentHashMap<Long, String> UserTokenPool = new ConcurrentHashMap<Long, String>();
	
	/** 玩家基本資料*/
	private static ConcurrentHashMap<Long, UserData> UserDataPool = new ConcurrentHashMap<Long, UserData>();

	
	/** 玩家道具資訊*/
	
	/** 玩家關卡資訊*/
	
	/** 玩家好友資訊*/
	
	/** 玩家排名*/
	
	/** <b>玩家封包處理暫存<br>
	 * Long:玩家編號<br>
	 * Integer:封包編號<br>
	 * JSONObject:封包內容
	 * */
	private static ConcurrentHashMap<Long,TreeMap<Integer,JSONObject>> 
		PacketTemp = new ConcurrentHashMap<Long, TreeMap<Integer,JSONObject>>();
	
	
//===================================================================================================================
	/**
	 * 新增封包至暫存區
	 * @param userId Long 玩家編號
	 * @param packetId Integer 封包編號(由client定義) 
	 * @param packet JSONObject 封包內容
	 * */
	public void addDataToPacketTemp(Long userId, Integer packetId ,JSONObject packet)
	{
		if(PacketTemp.containsKey(userId))
		{
			TreeMap<Integer, JSONObject> temp = PacketTemp.get(userId);
			if(temp.size()>=50)//最多保留50筆
			{
				temp.remove(temp.firstKey());
			}
			temp.put(packetId, packet);
		}
		else
		{
			TreeMap<Integer, JSONObject> container = new TreeMap<Integer, JSONObject>();
			container.put(packetId, packet);
			PacketTemp.put(userId, container);
		}
	}
	
	/**
	 * 移除暫存區中的封包
	 * @param userId Long 
	 * @return true:已從暫存區中移除,false:無對應的資料
	 * */
	public synchronized boolean removePacketTempAfterLogin(Long userId)
	{
		//玩家封包暫存
//		TreeMap<Integer, JSONObject> temp = PacketTemp.get(userId);
//		temp.clear();
		PacketTemp.get(userId).clear();
		return true;
	}
	
	/**
	 * 
	 * */
	public synchronized void removeAllPacketTempByUserId(Long userId)
	{
		PacketTemp.remove(userId);
	}
	
	
	/**
	 * 
	 * */
	public JSONObject getPacketTempData(Long userId,Integer packetId )
	{
		return PacketTemp.get(userId).get(packetId);
	}
//===================================================================================================================
	
	
	
	
	
}
