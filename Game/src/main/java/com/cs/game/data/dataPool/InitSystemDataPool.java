package com.cs.game.data.dataPool;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cs.beans.ApSellInfo;
import com.cs.beans.ApparelMakeInfo;
import com.cs.beans.ItemInfo;
import com.cs.beans.RoleApparelMapping;
import com.cs.beans.StageReward;
import com.cs.util.FileLoader;

/** 
 * 初始化系統資料
 */ 
@Component("InitSystemDataPool")
public class InitSystemDataPool implements InitializingBean{

	@Autowired
	private FileLoader fileLoader;
	
	private static final Logger LOG = LoggerFactory.getLogger(InitSystemDataPool.class);
	
	//購買buff的價格資訊
	private static ConcurrentHashMap<Integer, Integer> BuffPriceMap = new ConcurrentHashMap<Integer, Integer>();
	
	//服裝升級所需的素材
	private static ConcurrentHashMap<Integer, ArrayList<ApparelMakeInfo>> Apparel_MakeInfo_0 = new ConcurrentHashMap<Integer, ArrayList<ApparelMakeInfo>>();
	private static ConcurrentHashMap<Integer, ArrayList<ApparelMakeInfo>> Apparel_MakeInfo_1 = new ConcurrentHashMap<Integer, ArrayList<ApparelMakeInfo>>();
	private static ConcurrentHashMap<Integer, ArrayList<ApparelMakeInfo>> Apparel_MakeInfo_2 = new ConcurrentHashMap<Integer, ArrayList<ApparelMakeInfo>>();
	private static ConcurrentHashMap<Integer, ArrayList<ApparelMakeInfo>> Apparel_MakeInfo_3 = new ConcurrentHashMap<Integer, ArrayList<ApparelMakeInfo>>();
	private static ConcurrentHashMap<Integer, ArrayList<ApparelMakeInfo>> Apparel_MakeInfo_4 = new ConcurrentHashMap<Integer, ArrayList<ApparelMakeInfo>>();
	
	//素材資訊
	private static ConcurrentHashMap<Integer, ItemInfo> ItemMap = new ConcurrentHashMap<Integer, ItemInfo>();
	
	//角色對應服裝
	private static ConcurrentHashMap<Integer, RoleApparelMapping> RoleApparelMap = new ConcurrentHashMap<Integer, RoleApparelMapping>();
	//角色初始服裝
	private static ConcurrentHashMap<Integer,Integer> Role_Basic_Apparel_Map = new ConcurrentHashMap<Integer, Integer>();
	
	
	//AP相關
	/** 每次回復AP點數*/
	public static int RestoreAP = 10;//一次回復10點
	/**每次扣除AP點數*/
	public static int DeductionAp = 5;//每次扣除AP點數
	/** 每次回復AP時間*/
	private static final int MINUTE = 10;//分
	public static long RestoreAP_time = 1000*60*MINUTE;
	/** AP上限*/
	public static final int AP_LIMIT = 100;//分
	
	/** AP販售資訊*/
	private static ConcurrentHashMap<Integer,ApSellInfo> ApSellMap = new ConcurrentHashMap<Integer, ApSellInfo>();

	
	//關卡資訊
	/** 關卡分數星等對照*/
	private static ConcurrentHashMap<Integer, ArrayList<Integer>> ScoreLevelMap = new ConcurrentHashMap<Integer, ArrayList<Integer>>();
	/** 關卡獎勵對照_1星*/
	private static ConcurrentHashMap<Integer, ArrayList<StageReward>> StageRewardMap_1 = new ConcurrentHashMap<Integer, ArrayList<StageReward>>(); 
	/** 關卡獎勵對照_2星*/
	private static ConcurrentHashMap<Integer, ArrayList<StageReward>> StageRewardMap_2 = new ConcurrentHashMap<Integer, ArrayList<StageReward>>();
	/** 關卡獎勵對照_3星*/
	private static ConcurrentHashMap<Integer, ArrayList<StageReward>> StageRewardMap_3 = new ConcurrentHashMap<Integer, ArrayList<StageReward>>();
	
	/** 角色升級花費對照*/ 
	private static ConcurrentHashMap<Integer, Integer> RoleLevelMapping = new ConcurrentHashMap<Integer, Integer>();
	
	/** 輔助道具價格對照*/
	private static ConcurrentHashMap<Integer, Integer> ToolPriceMap = new ConcurrentHashMap<Integer, Integer>();
	
	//初始化順序  Constructor > @PostConstruct > InitializingBean > init-method
	//Spring 允许在 Bean 在初始化完成后以及 Bean 销毁前执行特定的操作，常用的设定方式有以下三种：
	//通过实现 InitializingBean/DisposableBean 接口来定制初始化之后/销毁之前的操作方法；
	//通过 <bean> 元素的 init-method/destroy-method属性指定初始化之后 /销毁之前调用的操作方法；
	//在指定方法上加上@PostConstruct 或@PreDestroy注解来制定该方法是在初始化之后还是销毁之前调用。
	
	/**
	 * 初始化方法
	 */
	public InitSystemDataPool() {
		LOG.info(" init InitSystemDataPool InitSystemDataPool ");
		LOG.debug("====> init InitSystemDataPool <====");
	}
	   
	/**
	 * spring 初始化方法
	 */
	@PostConstruct
	public void postConstruct() {
		LOG.info(" init InitSystemDataPool postConstruct ");
		LOG.debug("====> init postConstruct <====");
	}
	
	/**
	 * spring 初始化方法(afterPropertiesSet是直接执行的，效率較高但init-method消除了bean对Spring依赖)
	 */
	@Override
	public void afterPropertiesSet() throws Exception {//initializing
		LOG.info(" init InitSystemDataPool afterPropertiesSet ");
		LOG.debug("====> init afterPropertiesSet <====");
		init();
	}
	
	/**
	 * spring 初始化方法(init-method是通过反射執行的，效率比afterPropertiesSet差 )
	 */
	public void initMethod() {
		LOG.info(" init InitSystemDataPool initMethod ");
		LOG.debug("====> init initMethod <====");
	}
	
	/**
	 * spring 初始化方法(與PostConstruct 相反)
	 */
	@PreDestroy
	public void preDestroy() {
		LOG.info(" init InitSystemDataPool postConstruct ");
		LOG.debug("====> init preDestroy <====");
	}
	
	/**
	 * 自定義初始化方法
	 */
	public void init() throws Exception {
		LOG.info(" init InitSystemDataPool init() ");
//		System.out.println("初始化前: "+BuffPriceMap);
		
		initBuffPriceMap();
		initApparelMakeInfo();
		initItemMap();
		initRoleApparelMapping();
		initScoreLevelMap();
		initStageRewardMap();
		initRoleLevelMapping();
		initToolPriceMap();
		initApSellMap();
	}
	
	
//===================================================================================================================	
//	初始化資料
//===================================================================================================================	
	public void initBuffPriceMap() throws IOException
	{
		BuffPriceMap = fileLoader.initBuffPriceMap();
	}

	public void initApparelMakeInfo() throws IOException
	{
		Apparel_MakeInfo_0 = fileLoader.initApparelMakeInfo(0);
		Apparel_MakeInfo_1 = fileLoader.initApparelMakeInfo(1);
		Apparel_MakeInfo_2 = fileLoader.initApparelMakeInfo(2);
		Apparel_MakeInfo_3 = fileLoader.initApparelMakeInfo(3);
		Apparel_MakeInfo_4 = fileLoader.initApparelMakeInfo(4);
	}
	
	public void initItemMap() throws IOException
	{
		ItemMap = fileLoader.initItemInfo();
	}
	
	public void initRoleApparelMapping() throws IOException
	{
		RoleApparelMap = fileLoader.initRoleApparelMapping();
		
		for(Entry<Integer, RoleApparelMapping> e : RoleApparelMap.entrySet())
		{
			Role_Basic_Apparel_Map.put(e.getKey(), e.getValue().getCondition().get(0));
		}
	}
	
	public void initScoreLevelMap() throws IOException
	{
		ScoreLevelMap = fileLoader.initScoreLevelMap();
		
	}
	
	public void initStageRewardMap() throws IOException
	{
		StageRewardMap_1 = fileLoader.initStageRewardMap(1);
		StageRewardMap_2 = fileLoader.initStageRewardMap(2);
		StageRewardMap_3 = fileLoader.initStageRewardMap(3);
	}
	
	public void initRoleLevelMapping() throws IOException
	{
		RoleLevelMapping = fileLoader.initRoleLevelMapping();
	}
	
	public void initToolPriceMap() throws IOException
	{
		ToolPriceMap = fileLoader.initToolPriceMap();
	}
	
	public void initApSellMap() throws IOException
	{
		ApSellMap = fileLoader.initApSellMap();
		System.out.println("initApSellMap: "+ApSellMap);
	}
	
//===================================================================================================================	
//	get&set資料
//===================================================================================================================
	public ConcurrentHashMap<Integer, Integer> getBuffPriceMap()
	{
		return BuffPriceMap;
	}
	
	public ConcurrentHashMap<Integer, ArrayList<ApparelMakeInfo>> getApparelMakeInfo(int level)
	{
		switch(level)
		{
			case 0 :
				return Apparel_MakeInfo_0;
			case 1 :
				return Apparel_MakeInfo_1;
			case 2 :
				return Apparel_MakeInfo_2;
			case 3 :
				return Apparel_MakeInfo_3;
			case 4 :
				return Apparel_MakeInfo_4;
			default:
				return null;
		}
	}
	
	public ArrayList<ApparelMakeInfo> getApparelMakeInfo(int level, int apparelId)
	{
		switch(level)
		{
			case 0 :
				return Apparel_MakeInfo_0.get(apparelId);
			case 1 :
				return Apparel_MakeInfo_1.get(apparelId);
			case 2 :
				return Apparel_MakeInfo_2.get(apparelId);
			case 3 :
				return Apparel_MakeInfo_3.get(apparelId);
			case 4 :
				return Apparel_MakeInfo_4.get(apparelId);
			default:
				return null;
		}
	}
	
	/** 取得素材資訊*/
	public ItemInfo getIntemInfo(int itemId)
	{
		return ItemMap.get(itemId);
	}
	
	/** 取得角色對應的服裝編號list*/
	public RoleApparelMapping getRoleApparelMapping(int roleId)
	{
		return RoleApparelMap.get(roleId);
	}
	
	/** 取得角色基本服裝編號*/
	public Integer getRoleBasicApparelId(int roleId)
	{
		return Role_Basic_Apparel_Map.get(roleId);
	}
	
	/** 取得全部角色的基本服裝資訊*/
	public ConcurrentHashMap<Integer,Integer> getAll_RoleBasicApparelMap()
	{
		return Role_Basic_Apparel_Map;
	}
	
	
	/** 取得關卡星等對照*/
	public ConcurrentHashMap<Integer, ArrayList<Integer>> getScoreLevelMap()
	{
		return ScoreLevelMap;
	}
	
	/** 依星等取得關卡獎勵資訊
	 * @param int start 星等
	 * */
	public ArrayList<StageReward> getStageRewardMap(int start, int stageId)
	{
		switch(start)
		{
			case 1:
				return StageRewardMap_1.get(stageId);
			case 2:
				return StageRewardMap_2.get(stageId);
			case 3:
				return StageRewardMap_3.get(stageId);
			default:
				return null;
		}
	}
	
	/** 取得角色升級對應
	 * @param int level 等級
	 * @return Integer 升級所需的費用
	 * */
	public Integer getRoleLevelMapping(int level)
	{
		return RoleLevelMapping.get(level);
	}
	
	/**
	 * 取得輔助道具之價格
	 * @param Integer toolIndex 輔助道具的index 0開始
	 * @return Integer 價格
	 * */
	public Integer getToolPrice(int toolIndex)
	{
		return ToolPriceMap.get(toolIndex);
	}
	
	/** 取得AP販售資訊*/
	public ApSellInfo getApSellInfo(int id)
	{
		return ApSellMap.get(id);
	}
	
	
}
