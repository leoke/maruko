package com.cs.game.system.quartz;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class UserDataQuartz {
	
	private static final Logger	LOG	= LoggerFactory.getLogger(UserDataQuartz.class);
	
	/**
	 * 檢查玩家是否過久未對server發出請求,以判斷是否將其資料清空
	 * */
	public void checkUserIsOverTime()
	{
		
	}

}
