package com.cs.game.redis.service;

import com.cs.game.redis.beans.UserInfo;
import com.cs.model.csentity.UserData;

public interface UserOperationsService {
	/** 增加玩家資料,並且更新token*/
	void putUser(UserData user,String token);
	/** 增加玩家資料,並且更新token*/
	void putUser(UserData user);
	/** 增加Token*/
	void putToken(String uid,String token);
	/** 取得Token*/
	String getToken(String uid);
	/** 更新Token*/
	String updateToken(String uid,String token);
	/** 更新Token時效*/
	void updateTokenTimeLimit(String uid);
	/** 取得玩家資料*/
	UserData getUser(String key);
//	/** 取得玩家資料*/
//	UserData getUser(String uid,String token);
	/** 增加上一筆封包資料,token不更新,僅用來檢查是否為合法的token*/
	void putLastPacket(String uid,String token,String packet);
	/** 增加上一筆封包資料,不檢查token*/
	void putLastPacket(String uid,String packet);
	/** 取得上一筆封包資料,取得資料後不更新token*/
	String getLastPacket(String uid,String token);
	/** 取得上一筆封包資料,取得資料後不更新token*/
	String getLastPacket(String uid);
}
