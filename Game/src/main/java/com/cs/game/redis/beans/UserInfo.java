package com.cs.game.redis.beans;

import java.io.Serializable;
import java.util.Date;

import com.cs.model.csentity.UserData;
import com.cs.util.DateUtil;

/**
 * to client
 * */
public class UserInfo implements UserDataPool,Packet,Serializable{
	
	//玩家編號
	private Long uid;
	//登入類型, 0:快速,1:FB 
	private Integer loginMode;
	//遊戲幣
	private Integer gcoin;
	//珍珠?
	private Integer coin;
	//行動力(體力)
	private Integer ap;
	//行動力(體力)上次回復時間
	private String apTime;
	//關卡輔助道具
	private String tool;
	//關卡效果加成
	private String buff;
	
	//
//	private String token;
	
	public UserInfo()
	{
		apTime=DateUtil.getSDF_ToString(new Date());
		tool = "[0,0,0,0,0]";
		buff = "[0,0,0]";
	}
	public UserInfo(UserData ud)
	{
		this.uid =ud.getUid();
		this.loginMode=ud.getLoginMode();
		this.gcoin=ud.getGcoin();
		this.coin=ud.getCoin();
		this.ap=ud.getAp();
		this.apTime = DateUtil.getSDF_ToString(ud.getApTime().getTime());
		this.tool = ud.getTool();
		this.buff = ud.getBuff();
	}

	public UserInfo(UserData ud,String token)
	{
		this.uid =ud.getUid();
		this.loginMode=ud.getLoginMode();
		this.gcoin=ud.getGcoin();
		this.coin=ud.getCoin();
		this.ap=ud.getAp();
		this.apTime = DateUtil.getSDF_ToString(ud.getApTime().getTime());
		this.tool = ud.getTool();
		this.buff = ud.getBuff();
	}

	public Long getUid() {
		return uid;
	}


	public void setUid(Long uid) {
		this.uid = uid;
	}


	public Integer getLoginMode() {
		return loginMode;
	}


	public void setLoginMode(Integer loginMode) {
		this.loginMode = loginMode;
	}


	public Integer getGcoin() {
		return gcoin;
	}


	public void setGcoin(Integer gcoin) {
		this.gcoin = gcoin;
	}


	public Integer getCoin() {
		return coin;
	}


	public void setCoin(Integer coin) {
		this.coin = coin;
	}


	public Integer getAp() {
		return ap;
	}


	public void setAp(Integer ap) {
		this.ap = ap;
	}


	public String getApTime() {
		return apTime;
	}
	
	public void setApTime(String apTime) {
		this.apTime = apTime;
	}
	public String getTool() {
		return tool;
	}
	public void setTool(String tool) {
		this.tool = tool;
	}
	public String getBuff() {
		return buff;
	}
	public void setBuff(String buff) {
		this.buff = buff;
	}
	
}
