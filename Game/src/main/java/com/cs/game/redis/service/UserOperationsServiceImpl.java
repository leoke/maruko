package com.cs.game.redis.service;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.stereotype.Service;

import com.cs.game.redis.beans.UserDataPool;
import com.cs.game.redis.beans.UserInfo;
import com.cs.model.csentity.UserData;
import com.cs.util.RedisDefind;
import com.cs.util.TokenUtil;


@Service
@SuppressWarnings({"rawtypes","unchecked"})
public class UserOperationsServiceImpl implements UserOperationsService {
	
	private static final Logger	LOG	= LoggerFactory.getLogger(UserOperationsServiceImpl.class);
	
	private static final long LIMIT_TIME = 1800l;
	@Autowired
	private RedisTemplate redisTemplate;
	@Autowired
	private TokenUtil tokenUtil;
	
	
	@Override
	public void putUser(UserData user,String token) 
	{
		ValueOperations<String, UserData> valueops = redisTemplate
				.opsForValue();
		valueops.set(user.getUid().toString(), user);
		redisTemplate.expire(user.getUid().toString(), LIMIT_TIME, TimeUnit.SECONDS);
		//新增token
		this.putToken(user.getUid().toString(),token);
	}
	
	@Override
	public void putUser(UserData user) 
	{
		ValueOperations<String, UserData> valueops = redisTemplate
				.opsForValue();
		valueops.set(user.getUid().toString(), user);
		redisTemplate.expire(user.getUid().toString(), LIMIT_TIME, TimeUnit.SECONDS);
	}
	
	@Override
	public String updateToken(String uid, String token) 
	{
		ValueOperations<String, ConcurrentHashMap<String, UserDataPool>> valueops = redisTemplate
				.opsForValue();
		ConcurrentHashMap<String, UserDataPool> userData = valueops.get(uid);
		if(userData==null)
		{
			LOG.info("uid: "+uid+" updateToken fail!!!");
			return "-1";
		}
		UserInfo userInfo = (UserInfo) userData.get(RedisDefind.USERDATA);
		userData.put(RedisDefind.USERDATA, userInfo);
		valueops.set(uid, userData);
		redisTemplate.expire(uid, LIMIT_TIME, TimeUnit.SECONDS);
		return token;
	}
	

	@Override
	public void updateTokenTimeLimit(String uid) 
	{
		redisTemplate.expire(uid, LIMIT_TIME, TimeUnit.SECONDS);
	}

	@Override
	public UserData getUser(String key) 
	{
		ValueOperations<String, UserData> valueops = redisTemplate.opsForValue();
		return valueops.get(key);
	}

//	@Override
//	public UserData getUser(String id, String token) 
//	{
//		ValueOperations<String, ConcurrentHashMap<String, UserDataPool>> valueops = redisTemplate
//				.opsForValue();
//		ConcurrentHashMap<String, UserDataPool> map = valueops.get(id);
//		UserInfo userInfo = (UserInfo) map.get(RedisDefind.USERDATA);
//		if(userInfo==null) return null;
////		String _token = userInfo.getToken();
////		if(!_token.equals(token)) return null;
//		return userInfo;
//	}


	@Override
	public void putToken(String uid, String token) 
	{
		ValueOperations<String, String> valueops = redisTemplate.opsForValue();
		String _uidKey = uid+RedisDefind.TOKEN;
		valueops.set(_uidKey, token);
		redisTemplate.expire(_uidKey, LIMIT_TIME, TimeUnit.SECONDS);
	}

	@Override
	public String getToken(String uid) 
	{
		ValueOperations<String, String> valueops = redisTemplate.opsForValue();
		return valueops.get(uid+RedisDefind.TOKEN);
	}

	@Override
	public void putLastPacket(String uid, String token,String packet) 
	{
		//檢查token是否合法
		if(this.getToken(uid)==null)return;
		if(!this.getToken(uid).equals(token))return;
		ValueOperations<String, String> valueops = redisTemplate.opsForValue();
		valueops.set(uid+RedisDefind.LAST_PACKET, packet);
		redisTemplate.expire(uid+RedisDefind.LAST_PACKET, LIMIT_TIME, TimeUnit.SECONDS);
	}

	@Override
	public void putLastPacket(String uid, String packet) 
	{
		if(this.getToken(uid)==null)return;
		ValueOperations<String, String> valueops = redisTemplate.opsForValue();
		valueops.set(uid+RedisDefind.LAST_PACKET, packet);
		redisTemplate.expire(uid+RedisDefind.LAST_PACKET, LIMIT_TIME, TimeUnit.SECONDS);
	}

	@Override
	public String getLastPacket(String uid, String token) 
	{
		//檢查token是否合法
		if(this.getToken(uid)==null)return null;
		if(!this.getToken(uid).equals(token))return null;
		ValueOperations<String, String> valueops = redisTemplate.opsForValue();
		return valueops.get(uid+RedisDefind.LAST_PACKET);
	}


	@Override
	public String getLastPacket(String uid) 
	{
		ValueOperations<String, String> valueops = redisTemplate.opsForValue();
		return valueops.get(uid+RedisDefind.LAST_PACKET);
	}

}
