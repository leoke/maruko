package com.cs.game.aop.aspect;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cs.game.handler.UserHandler;
import com.cs.game.redis.beans.UserInfo;
import com.cs.game.redis.service.UserOperationsServiceImpl;

@Aspect
@Component
public class TokenAspect {
	private static final Logger	LOG	= LoggerFactory.getLogger(TokenAspect.class);
	
//	@Autowired
//	private UserHandler userHandler; 
	@Autowired 
	private UserOperationsServiceImpl userOperationsService;
	/**
	 * 在method執行之前會先執行此段程式
	 * 
	 * @param joinPoint
	 */
//	@Before("execution(* com.cs.game.redis.service.UserOperationsServiceImpl.put*(..)) ")
//	@Before("execution(* com.cs.game.handler.UserHandler.*(..)) ")
//	public void before(JoinPoint joinPoint) 
//	{
//		System.out.println("------------before");
//	}
	
    @AfterReturning(
            pointcut = "execution(* com.cs.game.redis.service.UserOperationsServiceImpl.put*(..))",
            returning= "result")
    public void afterMethodReturning(JoinPoint joinPoint, Object result) 
    {
    	System.out.println("------------afterMethodReturning");
    	System.out.println("result>>>"+result);
    	System.out.println("joinPoint>>>"+joinPoint.getArgs());
    	Object[] args = joinPoint.getArgs();
		for(int i = 0 ;i < args.length ; i++)
		{
			System.out.println("params["+i+"]:"+ args[i].toString());
		}
		try
		{
			UserInfo uInfo = (UserInfo)args[0];
			userOperationsService.updateTokenTimeLimit(uInfo.getUid().toString());
			JSONObject rs = JSONObject.fromObject(args[1]);
			if(rs.getInt("rs")==0)
			{
				userOperationsService.updateTokenTimeLimit(uInfo.getUid().toString());
			}
			
		}
		catch(Exception e)
		{
			String uid = args[0].toString();
			userOperationsService.updateTokenTimeLimit(uid);
			return;
		}
		return;
    }
}
