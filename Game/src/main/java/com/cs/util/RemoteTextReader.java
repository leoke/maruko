package com.cs.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**   
 * 取得遠端文件檔案內容 
 */ 
@Component("RemoteTextReader")
public class RemoteTextReader {
	
	private static final Logger LOG = LoggerFactory.getLogger(RemoteTextReader.class);
	
	/**   
     * 取得遠端文件檔案內容 
     * @param urlAddress 檔案位置
     * @param fileName 檔案名稱
     * @return List<String> 檔案每行內容
	 * @throws IOException 
     */ 
	public List<String> getRemoteTextData(String urlAddress, String fileName) throws IOException {
		  
		List<String> list = new ArrayList<String>();
		InputStream inStream;
		BufferedReader br = null;
		try {
			URL url = new URL(urlAddress+fileName);
		  	URLConnection connection = url.openConnection();
		  	connection.setDoInput(true);
		  	inStream = connection.getInputStream();
		  	br = new BufferedReader(new InputStreamReader(inStream));
		  	String line;
		  	
		  	while ((line = br.readLine()) != null) {    
		  		list.add(line);
		  	}
		   
		}catch (MalformedURLException e) {
			e.printStackTrace();
		}catch (IOException e) {
			e.printStackTrace();
		}finally{
			br.close();
		}
		return list;
	}
}
