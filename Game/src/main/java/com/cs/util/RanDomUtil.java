package com.cs.util;

import org.springframework.stereotype.Component;

@Component("RanDomUtil")
public class RanDomUtil 
{
	private static final int START =0;
	private static final int END = 1000;
	
	/**
	 * 檢查是否可以獲得道具<br>
	 * true:可獲得,false:失敗
	 * */
	public static boolean check(int number)
	{
		int i = (int)(Math.random()*(END-START+1))+START;
		if(number>=i)return true;
		return false;
	}
}
