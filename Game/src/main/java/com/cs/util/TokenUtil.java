package com.cs.util;

import java.math.BigInteger;
import java.security.SecureRandom;

import net.sf.json.JSONObject;

import org.springframework.stereotype.Component;

@Component("TokenUtil")
public class TokenUtil {
	
	private static final SecureRandom RANDOM = new SecureRandom();
	private static final int NUMBITS = 160;
	private static final int RADIX = 36;
	
	
	
	
	public String getToken()
	{
		return new BigInteger(NUMBITS, RANDOM).toString(RADIX);
	}
	
	public JSONObject setNewToken(JSONObject json)
	{
		json.put(RedisDefind.TOKEN, this.getToken());
		return json;
	}
}
