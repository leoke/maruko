package com.cs.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import com.cs.beans.ApSellInfo;
import com.cs.beans.ApparelMakeInfo;
import com.cs.beans.ItemInfo;
import com.cs.beans.RoleApparelMapping;
import com.cs.beans.StageReward;

@PropertySource(value={"classpath:server.properties"})
@Component("FileLoader")
public class FileLoader {
	
	private static final Logger LOG = LoggerFactory.getLogger(FileLoader.class);
	private static final String SPLITWORD = "\t";
	@Autowired
	private RemoteTextReader fileReader;
	@Value("${file.url}")  
	private String ConfigURL;
	
	public List<String> test(String fileName) throws IOException
	{
		
		List<String> dataTemp =  fileReader.getRemoteTextData(ConfigURL, fileName);
		for(int i=0;i<dataTemp.size();i++)
		{
			String _lineData = dataTemp.get(i);
			String[] _data = _lineData.split(SPLITWORD);
			System.out.println("id: "+_data[0]+"  name: "+_data[1]);
		}
		return null;
	}
	
	public ConcurrentHashMap<Integer, Integer> initBuffPriceMap() throws IOException
	{
		ConcurrentHashMap<Integer, Integer> map = new ConcurrentHashMap<Integer, Integer>();
		List<String> dataTemp =  fileReader.getRemoteTextData(ConfigURL, "buffList.txt");
		for(int i=1;i<dataTemp.size();i++)
		{
			String _lineData = dataTemp.get(i);
			String[] _data = _lineData.split(SPLITWORD);
			map.put(Integer.parseInt(_data[0]), Integer.parseInt(_data[2]));
		}
		return map;
	}
	
	/** 文件apparel_0的編碼,已apparel_0為例即傳入0*/
	public ConcurrentHashMap<Integer, ArrayList<ApparelMakeInfo>> initApparelMakeInfo(Integer number) throws IOException
	{
		ConcurrentHashMap<Integer, ArrayList<ApparelMakeInfo>> map = new ConcurrentHashMap<Integer, ArrayList<ApparelMakeInfo>>();
		List<String> dataTemp =  fileReader.getRemoteTextData(ConfigURL, "apparel_"+number+".txt");
		for(int i=1;i<dataTemp.size();i++)
		{
			String _lineData = dataTemp.get(i);
			String[] _data = _lineData.split(SPLITWORD);
			ArrayList<ApparelMakeInfo> data = new ArrayList<ApparelMakeInfo>();
			
			if(_data[2].length()!=0)//遊戲幣
			{
				ApparelMakeInfo _info = new ApparelMakeInfo();
				_info.setType(1);//type1遊戲幣
				_info.setAmount(Integer.parseInt(_data[2]));//遊戲幣的類型
				data.add(_info);
			}
			
			for(int j=3;j<_data.length;j=j+2)
			{
				ApparelMakeInfo _info = new ApparelMakeInfo();
				_info.setType(10);//type10 道具
				_info.setItemId(Integer.parseInt(_data[j]));
				_info.setAmount(Integer.parseInt(_data[j+1]));
				data.add(_info);
			}
			map.put(Integer.parseInt(_data[0]),data);
		}
//		System.out.println(info);
		return map;
	}
	
	/**
	 * 素材
	 * */
	public ConcurrentHashMap<Integer, ItemInfo> initItemInfo() throws IOException
	{
		ConcurrentHashMap<Integer, ItemInfo> map = new ConcurrentHashMap<Integer,ItemInfo>();
		List<String> dataTemp =  fileReader.getRemoteTextData(ConfigURL, "itemInfo.txt");
		
		for(int i=1;i<dataTemp.size();i++)
		{
			String _lineData = dataTemp.get(i);
			String[] _data = _lineData.split(SPLITWORD);
			ItemInfo item = new ItemInfo();
			item.setItemId(Integer.parseInt(_data[0]));
			item.setType(Integer.parseInt(_data[2]));
			item.setAmount(Integer.parseInt(_data[3]));
			item.setPrice(Integer.parseInt(_data[4]));
			map.put(Integer.parseInt(_data[0]),item);
		}
		
		return map;
	}
	
	/** 角色對應服裝資訊*/
	public ConcurrentHashMap<Integer, RoleApparelMapping> initRoleApparelMapping() throws IOException
	{
		ConcurrentHashMap<Integer, RoleApparelMapping> map = new ConcurrentHashMap<Integer, RoleApparelMapping>();
		List<String> dataTemp =  fileReader.getRemoteTextData(ConfigURL, "roleApparel.txt");
		for(int i=1;i<dataTemp.size();i++)
		{
			String _lineData = dataTemp.get(i);
			String[] _data = _lineData.split(SPLITWORD);
			String condition = _data[2];
			String[] apparel = condition.split(",");
			ArrayList<Integer> temp = new ArrayList<Integer>();
			for(int j =0;j< apparel.length;j++)
			{
				temp.add(Integer.parseInt(apparel[j].trim()));
			}
			RoleApparelMapping rm = new RoleApparelMapping();
			rm.setCondition(temp);
			rm.setRoleId((Integer.parseInt(_data[0])));
			map.put(Integer.parseInt(_data[0]), rm);
		}	
		return map;
	}
	
	public ConcurrentHashMap<Integer, ArrayList<Integer>> initScoreLevelMap() throws IOException
	{
		ConcurrentHashMap<Integer, ArrayList<Integer>> map = new ConcurrentHashMap<Integer, ArrayList<Integer>>();
		List<String> dataTemp =  fileReader.getRemoteTextData(ConfigURL, "Scene_Info.txt");
		for(int i=1;i<dataTemp.size();i++)
		{
			String _lineData = dataTemp.get(i);
			String[] _data = _lineData.split(SPLITWORD);
			ArrayList<Integer> list = new ArrayList<Integer>();
			String[] levelList = _data[4].split(",");
			for(int j =0;j< levelList.length;j++)
			{
				list.add(Integer.parseInt(levelList[j]));
			}
			map.put((Integer.parseInt(_data[0])), list);
		}
		return map;
	}
	
	public ConcurrentHashMap<Integer, ArrayList<StageReward>> initStageRewardMap(int start) throws IOException
	{
		ConcurrentHashMap<Integer, ArrayList<StageReward>> map = new ConcurrentHashMap<Integer, ArrayList<StageReward>>();
		List<String> dataTemp =  fileReader.getRemoteTextData(ConfigURL, "stageReward_"+start+".txt");
	
		for(int i=1;i<dataTemp.size();i++)
		{
			String _lineData = dataTemp.get(i);
			String[] _data = _lineData.split(SPLITWORD);
			ArrayList<StageReward> list = new ArrayList<StageReward>();
			
			for(int j=2;j<_data.length;j=j+3)
			{
				if(_data[j].trim().length()==0)break;
				StageReward sr = new StageReward();
				sr.setItemId(Integer.parseInt(_data[0]));
				sr.setItemId(Integer.parseInt(_data[j]));
				sr.setAmount(Integer.parseInt(_data[j+1]));
				sr.setRate(Integer.parseInt(_data[j+2]));
				list.add(sr);
			}
			map.put(Integer.parseInt(_data[0]), list);
		}
//			System.out.println(start+" map 讀取資料"+map);
		return map;
	}
	
	public ConcurrentHashMap<Integer,Integer> initRoleLevelMapping() throws IOException
	{
		ConcurrentHashMap<Integer,Integer> map = new ConcurrentHashMap<Integer, Integer>();
		List<String> dataTemp =  fileReader.getRemoteTextData(ConfigURL, "roleLevelMapping.txt");
		for(int i=1;i<dataTemp.size();i++)
		{
			String _lineData = dataTemp.get(i);
			String[] _data = _lineData.split(SPLITWORD);
			map.put(Integer.parseInt(_data[0]), Integer.parseInt(_data[1]));
		}
		return map;
	}
	
	public ConcurrentHashMap<Integer, Integer> initToolPriceMap() throws IOException
	{
		ConcurrentHashMap<Integer, Integer> map = new ConcurrentHashMap<Integer, Integer>();
		List<String> dataTemp =  fileReader.getRemoteTextData(ConfigURL, "toolPrice.txt");
		for(int i=1;i<dataTemp.size();i++)
		{
			String _lineData = dataTemp.get(i);
			String[] _data = _lineData.split(SPLITWORD);
			map.put(i-1, Integer.parseInt(_data[2]));
		}
		return map;
	}
	
	public ConcurrentHashMap<Integer,ApSellInfo> initApSellMap() throws IOException
	{
		ConcurrentHashMap<Integer,ApSellInfo> map = new ConcurrentHashMap<Integer, ApSellInfo>();
		List<String> dataTemp =  fileReader.getRemoteTextData(ConfigURL, "apSellInfo.txt");
		for(int i=1;i<dataTemp.size();i++)
		{
			String _lineData = dataTemp.get(i);
			String[] _data = _lineData.split(SPLITWORD);
			ApSellInfo _info = new ApSellInfo();
			_info.setId(Integer.parseInt(_data[0]));
			_info.setAp(Integer.parseInt(_data[1]));
			_info.setPrice(Integer.parseInt(_data[2]));
			map.put(Integer.parseInt(_data[0]), _info);
		}
		return map;
	}
	
}
