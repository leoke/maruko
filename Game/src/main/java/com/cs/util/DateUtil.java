package com.cs.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {
	
	public static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	/**
	 * 取得格式化後的日期<br>
	 * mode:1=yyyy-MM-dd HH:mm:ss<br>
	 * */
	public static String getSDF_ToString(Date date)
	{
		return SDF.format(date);
	}
	
	/**
	 * 取得格式化後的日期<br>
	 * mode:1=yyyy-MM-dd HH:mm:ss<br>
	 * */
	public static String getSDF_ToString(Long date)
	{
		return SDF.format(new Date(date));
	}
	
	/**
	 * 字串(yyyy-MM-dd HH:mm:ss) 轉 long
	 * */
	public static long getSDF_ToLong(String formatDate)
	{
		try {
			return SDF.parse(formatDate).getTime();
		}
		catch (ParseException e) {
			e.printStackTrace();
		}
		return 0l;
	}
}
