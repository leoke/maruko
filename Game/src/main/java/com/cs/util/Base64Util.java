package com.cs.util;

import java.util.Base64;

import net.sf.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component("Base64Util")
public class Base64Util {
	
//	private static final Logger LOG = LoggerFactory.getLogger(Base64Util.class);
	
	final static Base64.Decoder Decoder = Base64.getDecoder();
	final static Base64.Encoder Encoder = Base64.getEncoder();
	
	/** base64編碼*/
	public String encodeToString(String json) throws Exception
	{
		return Encoder.encodeToString(json.getBytes("UTF-8"));
	}
	
	/** base64解碼*/
	public JSONObject decoderToJSONObject(String msg) throws Exception
	{
		return JSONObject.fromObject(new String(Decoder.decode(msg),"UTF-8"));
	}
	
}
