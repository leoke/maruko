package com.cs.model.springsecurity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the SecurityGroup database table.
 * 
 */
@Entity
@NamedQuery(name="SecurityGroup.findAll", query="SELECT g FROM SecurityGroup g")
public class SecurityGroup implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int groupId;

	private String groupDesc;

	private String groupName;

	private int groupStatus;

	@Lob
	private String pages;

	public SecurityGroup() {
	}

	public int getGroupId() {
		return this.groupId;
	}

	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}

	public String getGroupDesc() {
		return this.groupDesc;
	}

	public void setGroupDesc(String groupDesc) {
		this.groupDesc = groupDesc;
	}

	public String getGroupName() {
		return this.groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public int getGroupStatus() {
		return this.groupStatus;
	}

	public void setGroupStatus(int groupStatus) {
		this.groupStatus = groupStatus;
	}

	public String getPages() {
		return this.pages;
	}

	public void setPages(String pages) {
		this.pages = pages;
	}

}