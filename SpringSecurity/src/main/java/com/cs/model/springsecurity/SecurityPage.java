package com.cs.model.springsecurity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the SecurityPage database table.
 * 
 */
@Entity
@NamedQuery(name="SecurityPage.findAll", query="SELECT p FROM SecurityPage p")
public class SecurityPage implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int pageId;

	private String pageName;

	private int pageSwitch;

	private int sort;

	private int type;

	private String viewIcon;

	private String viewUrl;

	public SecurityPage() {
	}

	public int getPageId() {
		return this.pageId;
	}

	public void setPageId(int pageId) {
		this.pageId = pageId;
	}

	public String getPageName() {
		return this.pageName;
	}

	public void setPageName(String pageName) {
		this.pageName = pageName;
	}

	public int getPageSwitch() {
		return this.pageSwitch;
	}

	public void setPageSwitch(int pageSwitch) {
		this.pageSwitch = pageSwitch;
	}

	public int getSort() {
		return this.sort;
	}

	public void setSort(int sort) {
		this.sort = sort;
	}

	public int getType() {
		return this.type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getViewIcon() {
		return this.viewIcon;
	}

	public void setViewIcon(String viewIcon) {
		this.viewIcon = viewIcon;
	}

	public String getViewUrl() {
		return this.viewUrl;
	}

	public void setViewUrl(String viewUrl) {
		this.viewUrl = viewUrl;
	}

}